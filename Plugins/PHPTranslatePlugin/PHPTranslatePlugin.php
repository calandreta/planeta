<?php
/* Copyright (C) 2012 Calandreta Del Pa�s Murethin
 *
 * This file is part of CanteenCalandreta.
 *
 * CanteenCalandreta is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CanteenCalandreta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CanteenCalandreta; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * PHP plugin planning nursery auto save module : when the user check/uncheck a checkbox
 * in the planning, the nursery registration is auto save/deleted in the database
 *
 * @author Christophe Javouhey
 * @version 2.0
 *     - 2014-01-02 : v1.1. Taken into account english language
 *     - 2014-01-10 : v1.2. For the "delete" action, we add some "if" to check the content of variables
 *                    (must match with content database)
 *     - 2014-03-31 : v1.3. Taken into account Occitan language
 *     - 2016-06-20 : v1.4. Taken into account $CONF_CHARSET
 *     - 2022-11-10 : v2.0. Change the way to get translations (use $LANG_* variables instead of PHP lang file)
 *                    Taken into account translations coming from DB
 *
 * @since 2013-09-12
 */


 $CONF_LANG_OF_TRANSLATION_TRANSLATE_PLUGIN = 'fr';
 $CONF_MIN_LENGTH_TRANSLATE_PLUGIN          = 10;


 // Include Config.php because of the name of the session
 require '../../GUI/GraphicInterface.php';

 if (array_key_exists('MsgToTranslate', $_GET))
 {
     // Gte the text to translate
     $sMsgToTranslate = strTolower(trim(strip_tags(stripslashes(rawurldecode($_GET['MsgToTranslate'])))));
     if (ord(substr($sMsgToTranslate, -1)) == 160)
     {
         // Remove this character
         $sMsgToTranslate = trim(substr($sMsgToTranslate, 0, -1));
     }

     if (substr($sMsgToTranslate, -1) == '*')
     {
         // Remove this character
         $sMsgToTranslate = trim(substr($sMsgToTranslate, 0, -1));
     }

     if (substr($sMsgToTranslate, -1) == ':')
     {
         // Remove this character
         $sMsgToTranslate = trim(substr($sMsgToTranslate, 0, -1));
     }

     if (!empty($sMsgToTranslate))
     {
         // Get the translation file of the original language : we load all language variables
         $ArrayOriginalLangFile = array();
         $sPrefix = 'LANG_';
         $iNbChars = strlen($sPrefix);
         foreach($GLOBALS as $VarName => $VarValue)
         {
             if (substr($VarName, 0, $iNbChars) == $sPrefix)
             {
                 $ArrayOriginalLangFile[] = $VarName;
             }
         }

         // We detect the charset : it must be ISO-8859-1 (remove comment if there is a problem with chars with accents !!!)
         /*if ((strToUpper($CONF_CHARSET) == 'ISO-8859-1') && (mb_detect_encoding($sMsgToTranslate, 'UTF-8') == 'UTF-8'))
         {
             $sMsgToTranslate = utf8_decode($sMsgToTranslate);
         } */

         // Search the translation of the text
         $Index = NULL;
         $ArrayPossibleIndexes = array();
         foreach($ArrayOriginalLangFile as $i => $VarName)
         {
             if (stripos($GLOBALS[$VarName], $sMsgToTranslate) !== FALSE)
             {
                 $ArrayPossibleIndexes[] = $VarName;
             }
             elseif ((stripos($sMsgToTranslate, $GLOBALS[$VarName]) !== FALSE) && (strlen($GLOBALS[$VarName]) >= $CONF_MIN_LENGTH_TRANSLATE_PLUGIN))
             {
                 // In the case of the displayed message contents computed values (ex : W45-2014)
                 $ArrayPossibleIndexes[] = $VarName;
             }
         }

         // If several possibilities, find the nearst
         $Index = -1;
         if (!empty($ArrayPossibleIndexes))
         {
             $fBestDiff = 0.0;
             foreach($ArrayPossibleIndexes as $i => $VarName)
             {
                 $Diff = similar_text($GLOBALS[$VarName], $sMsgToTranslate, $fPercent);
                 if ($fPercent > $fBestDiff)
                 {
                     $fBestDiff = $fPercent;
                     $Index = $i;
                     if ($fBestDiff == 100)
                     {
                         // Stop the search
                         break;
                     }
                 }
             }
         }

         if ($Index >= 0)
         {
             // Connection to the database
             $DbCon = dbConnection();

             $ArrayTranslationLangFile = dbSearchConfigLanguages($DbCon,
                                                                 array(
                                                                       'ConfigLanguageName' => $ArrayPossibleIndexes[$Index],
                                                                       'ConfigLanguageLang' => $CONF_LANG_OF_TRANSLATION_TRANSLATE_PLUGIN
                                                                      ), "ConfigLanguageID", 1, 0);

             // Release the connection to the database
             dbDisconnection($DbCon);

             if ((isset($ArrayTranslationLangFile['ConfigLanguageID'])) && (!empty($ArrayTranslationLangFile['ConfigLanguageID'])))
             {
                 // We uese the translation coming from the DB
                 $TranslatedMessage = $ArrayTranslationLangFile['ConfigLanguageValue'][0];
             }
             else
             {
                 // Use the PHP lang file to get the translation
                 switch($CONF_LANG_OF_TRANSLATION_TRANSLATE_PLUGIN)
                 {
                     case 'fr':
                         $ArrayTranslationLangFile = file("../../Languages/Francais.lang.php");
                         break;

                     case 'oc':
                         $ArrayTranslationLangFile = file("../../Languages/Occitan.lang.php");
                         break;

                     default:
                         $ArrayTranslationLangFile = file("../../Languages/English.lang.php");
                         break;
                 }

                 foreach($ArrayTranslationLangFile as $i => $CurrentMessage)
                 {
                     if (strpos($CurrentMessage, $ArrayPossibleIndexes[$Index]) !== FALSE)
                     {
                         $iPosEqual = strpos($CurrentMessage, '=');
                         $TranslatedMessage = substr($CurrentMessage, $iPosEqual + 3, -4);
                         break;
                     }
                 }
             }

             // Send the translation
             header("Content-type: text/html; charset=".strtolower($CONF_CHARSET));
             echo $TranslatedMessage;

             unset($ArrayOriginalLangFile, $ArrayTranslationLangFile, $ArrayPossibleIndexes);
         }
         else
         {
             header("Content-type: text/html; charset=".strtolower($CONF_CHARSET));
             echo '503';
         }
     }
     else
     {
         header("Content-type: text/html; charset=".strtolower($CONF_CHARSET));
         echo '503';
     }
 }
 else
 {
     header("Content-type: text/html; charset=".strtolower($CONF_CHARSET));
     echo '503';
 }
?>
