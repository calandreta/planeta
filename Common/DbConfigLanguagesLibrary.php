<?php
/* Copyright (C) 2012 Calandreta Del Pa�s Murethin
 *
 * This file is part of CanteenCalandreta.
 *
 * CanteenCalandreta is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CanteenCalandreta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CanteenCalandreta; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * Common module : library of database functions used for the configuration languages table
 *
 * @author Christophe Javouhey
 * @version 3.9
 * @since 2022-10-17
 */


/**
 * Check if a config translation message exists in the ConfigLanguages table, thanks to its ID
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-17
 *
 * @param $DbConnection         DB object    Object of the opened database connection
 * @param $ConfigLanguageID     Integer      ID of the config translation message searched [1..n]
 *
 * @return Boolean              TRUE if the config translation message exists, FALSE otherwise
 */
 function isExistingConfigLanguage($DbConnection, $ConfigLanguageID)
 {
     $DbResult = $DbConnection->query("SELECT ConfigLanguageID FROM ConfigLanguages
                                       WHERE ConfigLanguageID = $ConfigLanguageID");
     if (!DB::isError($DbResult))
     {
         if ($DbResult->numRows() == 1)
         {
             // The config translation message exists
             return TRUE;
         }
     }

     // The config translation message doesn't exist
     return FALSE;
 }


/**
 * Give the ID of a config translation message thanks to its reference
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-17
 *
 * @param $DbConnection           DB object    Object of the opened database connection
 * @param $ConfigLanguageName     String       Name of the config translation message searched
 *
 * @return Integer                ID of the config translation message, 0 otherwise
 */
 function getConfigLanguageID($DbConnection, $ConfigLanguageName)
 {
     $DbResult = $DbConnection->query("SELECT ConfigLanguageID FROM ConfigLanguages
                                       WHERE ConfigLanguageName = \"$ConfigLanguageName\"");
     if (!DB::isError($DbResult))
     {
         if ($DbResult->numRows() != 0)
         {
             $Record = $DbResult->fetchRow(DB_FETCHMODE_ASSOC);
             return $Record["ConfigLanguageID"];
         }
     }

     // ERROR
     return 0;
 }


/**
 * Give the name of a config translation message thanks to its ID
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-17
 *
 * @param $DbConnection         DB object    Object of the opened database connection
 * @param $ConfigLanguageID     Integer      ID of the config translation message searched
 *
 * @return String               Name of the config translation message, empty string otherwise
 */
 function getConfigLanguageName($DbConnection, $ConfigLanguageID)
 {
     $DbResult = $DbConnection->query("SELECT ConfigLanguageName FROM ConfigLanguages
                                       WHERE ConfigLanguageID = $ConfigLanguageID");
     if (!DB::isError($DbResult))
     {
         if ($DbResult->numRows() != 0)
         {
             $Record = $DbResult->fetchRow(DB_FETCHMODE_ASSOC);
             return $Record["ConfigLanguageName"];
         }
     }

     // ERROR
     return "";
 }


/**
 * Add a config translation message in the ConfigParameters table
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-17
 *
 * @param $DbConnection                  DB object    Object of the opened database connection
 * @param $ConfigLanguageName            String       Name of the config translation message
 * @param $ConfigLanguageLang            String       Language of the config translation message (en, fr, ...)
 * @param $ConfigLanguageValue           String       Translation of the config message
 *
 * @return Integer                       The primary key of the config translation message [1..n], 0 otherwise
 */
 function dbAddConfigLanguage($DbConnection, $ConfigLanguageName, $ConfigLanguageLang, $ConfigLanguageValue = '')
 {
     if ((!empty($ConfigLanguageName)) && (!empty($ConfigLanguageLang)))
     {
         // Check if the config parameter is a new config translation message for the given language
         $DbResult = $DbConnection->query("SELECT ConfigLanguageID FROM ConfigLanguages
                                           WHERE ConfigLanguageName = \"$ConfigLanguageName\"
                                           AND ConfigLanguageLang = \"$ConfigLanguageLang\"");
         if (!DB::isError($DbResult))
         {
             if ($DbResult->numRows() == 0)
             {
                 // It's a new config translation message
                 $id = getNewPrimaryKey($DbConnection, "ConfigLanguages", "ConfigLanguageID");
                 if ($id != 0)
                 {
                     $DbResult = $DbConnection->query("INSERT INTO ConfigLanguages SET ConfigLanguageID = $id,
                                                       ConfigLanguageName = \"$ConfigLanguageName\",
                                                       ConfigLanguageLang = \"$ConfigLanguageLang\",
                                                       ConfigLanguageValue = \"".escapeSQLString($ConfigLanguageValue)."\"");

                     if (!DB::isError($DbResult))
                     {
                         return $id;
                     }
                 }
             }
             else
             {
                 // The config translation message already exists
                 $Record = $DbResult->fetchRow(DB_FETCHMODE_ASSOC);
                 return $Record['ConfigLanguageID'];
             }
         }
     }

     // ERROR
     return 0;
 }


/**
 * Update an existing config translation message in the ConfigLanguages table
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-17
 *
 * @param $DbConnection                  DB object    Object of the opened database connection
 * @param $ConfigLanguageID              Integer      ID of the config translation message to update [1..n]
 * @param $ConfigLanguageName            String       Name of the config translation message
 * @param $ConfigLanguageLang            String       Language of the config translation message (en, fr, ...)
 * @param $ConfigLanguageValue           String       Translation of the config message
 *
 * @return Integer                       The primary key of the config translation message [1..n], 0 otherwise
 */
 function dbUpdateConfigLanguage($DbConnection, $ConfigLanguageID, $ConfigLanguageName = NULL, $ConfigLanguageLang = NULL, $ConfigLanguageValue = NULL)
 {
     // The parameters which are NULL will be ignored for the update
     $ArrayParamsUpdate = array();

     // Verification of the parameters
     if (($ConfigLanguageID < 1) || (!isInteger($ConfigLanguageID)))
     {
         // ERROR
         return 0;
     }

     // Check if the ConfigLanguageName is valide
     if (!is_null($ConfigLanguageName))
     {
         if (empty($ConfigLanguageName))
         {
             return 0;
         }
         else
         {
             // The ConfigLanguageName field will be updated
             $ArrayParamsUpdate[] = "ConfigLanguageName = \"$ConfigLanguageName\"";
         }
     }
     else
     {
         // We get the name
         $ConfigLanguageName = getTableFieldValue($DbConnection, 'ConfigLanguages', $ConfigLanguageID, 'ConfigLanguageName');
     }

     // Check if the ConfigLanguageLang is valide
     if (!is_null($ConfigLanguageLang))
     {
         if (empty($ConfigLanguageLang))
         {
             return 0;
         }
         else
         {
             // The ConfigLanguageLang field will be updated
             $ArrayParamsUpdate[] = "ConfigLanguageLang = \"$ConfigLanguageLang\"";
         }
     }
     else
     {
         // We get the language
         $ConfigLanguageLang = getTableFieldValue($DbConnection, 'ConfigLanguages', $ConfigLanguageID, 'ConfigLanguageLang');
     }

     // Check if the ConfigLanguageValue is valide
     if (!is_null($ConfigLanguageValue))
     {
         // The ConfigLanguageValue field will be updated
         $ArrayParamsUpdate[] = "ConfigLanguageValue = \"".escapeSQLString($ConfigLanguageValue)."\"";
     }

     // Here, the parameters are correct, we check if the config translation message exists
     if (isExistingConfigLanguage($DbConnection, $ConfigLanguageID))
     {
         // We check if the name is unique for the given language
         $DbResult = $DbConnection->query("SELECT ConfigLanguageID FROM ConfigLanguages
                                           WHERE ConfigLanguageName = \"$ConfigLanguageName\"
                                           AND ConfigLanguageLang = \"$ConfigLanguageLang\"
                                           AND ConfigLanguageID <> $ConfigLanguageID");
         if (!DB::isError($DbResult))
         {
             if ($DbResult->numRows() == 0)
             {
                 // The config translation message exists and is unique : we can update if there is at least 1 parameter
                 if (count($ArrayParamsUpdate) > 0)
                 {
                     $DbResult = $DbConnection->query("UPDATE ConfigLanguages SET ".implode(", ", $ArrayParamsUpdate)
                                                      ." WHERE ConfigLanguageID = $ConfigLanguageID");
                     if (!DB::isError($DbResult))
                     {
                         // Config translation message updated
                         return $ConfigLanguageID;
                     }
                 }
                 else
                 {
                     // The update isn't usefull
                     return $ConfigLanguageID;
                 }
             }
         }
     }

     // ERROR
     return 0;
 }


/**
 * Give the whole fields values of a config translation message, thanks to his name
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-17
 *
 * @param $DbConnection              DB object    Object of the opened database connection
 * @param $ConfigLanguageName        String       Name of the config translation message searched
 * @param $ConfigLanguageLang        String       Language of the config translation message searched
 *                                                (en, fr, ...)
 *
 * @return Mixed array               All fields values of a config translation message if he exists,
 *                                   an empty array otherwise
 */
 function getConfigLanguageInfos($DbConnection, $ConfigLanguageName, $ConfigLanguageLang = 'en')
 {
     $DbResult = $DbConnection->query("SELECT ConfigLanguageID, ConfigLanguageName, ConfigLanguageLang, ConfigLanguageValue
                                       FROM ConfigLanguages
                                       WHERE ConfigLanguageName = \"$ConfigLanguageName\"
                                       AND ConfigLanguageLang = \"$ConfigLanguageLang\"");
     if (!DB::isError($DbResult))
     {
         if ($DbResult->numRows() != 0)
         {
             return $DbResult->fetchRow(DB_FETCHMODE_ASSOC);
         }
     }

     // ERROR
     return array();
 }


/**
 * Delete a config translation message, thanks to its ID.
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-17
 *
 * @param $DbConnection              DB object    Object of the opened database connection
 * @param $ConfigLanguageID          Integer      ID of the config translation message to delete [1..n]
 *
 * @return Boolean                   TRUE if the config translation message is deleted if it exists,
 *                                   FALSE otherwise
 */
 function dbDeleteConfigLanguage($DbConnection, $ConfigLanguageID)
 {
     // The parameters are correct?
     if ($ConfigLanguageID > 0)
     {
         // Delete the config translation message in the table
         $DbResult = $DbConnection->query("DELETE FROM ConfigLanguages WHERE ConfigLanguageID = $ConfigLanguageID");
         if (!DB::isError($DbResult))
         {
             // Config translation message deleted
             return TRUE;
         }
     }

     // ERROR
     return FALSE;
 }


/**
 * Get config translation messages filtered by some criterion
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-17
 *
 * @param $DbConnection             DB object              Object of the opened database connection
 * @param $ArrayParams              Mixed array            Contains the criterion used to filter the config translation messages
 * @param $OrderBy                  String                 Criteria used to sort the config translation messages. If < 0, DESC is used,
 *                                                         otherwise ASC is used
 * @param $Page                     Integer                Number of the page to return [1..n]
 * @param $RecordsPerPage           Integer                Number of config translation messages per page to return [1..n]
 *
 * @return Array of String                                 List of config translation messages filtered, an empty array otherwise
 */
 function dbSearchConfigLanguages($DbConnection, $ArrayParams, $OrderBy = "", $Page = 1, $RecordsPerPage = 10)
 {
     // SQL request to find config translation messages
     $Select = "SELECT cl.ConfigLanguageID, cl.ConfigLanguageName, cl.ConfigLanguageLang, cl.ConfigLanguageValue";
     $From = "FROM ConfigLanguages cl";
     $Where = "WHERE 1=1";
     $Having = "";

     if (count($ArrayParams) >= 0)
     {
         // <<< ConfigLanguageID field >>>
         if ((array_key_exists("ConfigLanguageID", $ArrayParams)) && (!empty($ArrayParams["ConfigLanguageID"])))
         {
             if (is_array($ArrayParams["ConfigLanguageID"]))
             {
                 $Where .= " AND cl.ConfigLanguageID IN ".constructSQLINString($ArrayParams["ConfigLanguageID"]);
             }
             else
             {
                 $Where .= " AND cl.ConfigLanguageID = ".$ArrayParams["ConfigLanguageID"];
             }
         }

         // <<< ConfigLanguageName field >>>
         if ((array_key_exists("ConfigLanguageName", $ArrayParams)) && (!empty($ArrayParams["ConfigLanguageName"])))
         {
             $Where .= " AND cl.ConfigLanguageName LIKE \"".$ArrayParams["ConfigLanguageName"]."\"";
         }

         // <<< ConfigLanguageLang field >>>
         if ((array_key_exists("ConfigLanguageLang", $ArrayParams)) && (!empty($ArrayParams["ConfigLanguageLang"])))
         {
             if (is_array($ArrayParams["ConfigLanguageLang"]))
             {
                 $Where .= " AND cl.ConfigLanguageLang IN ".constructSQLINString($ArrayParams["ConfigLanguageLang"]);
             }
             else
             {
                 $Where .= " AND cl.ConfigLanguageLang LIKE \"".$ArrayParams["ConfigLanguageLang"]."\"";
             }
         }

         // <<< ConfigLanguageValue field >>>
         if ((array_key_exists("ConfigLanguageValue", $ArrayParams)) && (!empty($ArrayParams["ConfigLanguageValue"])))
         {
             $Where .= " AND cl.ConfigLanguageValue LIKE \"".$ArrayParams["ConfigLanguageValue"]."\"";
         }
     }

     // We take into account the page and the number of config translation messages per page
     if ($Page < 1)
     {
         $Page = 1;
     }

     if ($RecordsPerPage < 0)
     {
         $RecordsPerPage = 10;
     }

     $Limit = '';
     if ($RecordsPerPage > 0)
     {
         $StartIndex = ($Page - 1) * $RecordsPerPage;
         $Limit = "LIMIT $StartIndex, $RecordsPerPage";
     }

     // We take into account the order by
     if ($OrderBy == "")
     {
         $StrOrderBy = "";
     }
     else
     {
         $StrOrderBy = " ORDER BY $OrderBy";
     }

     // We can launch the SQL request
     $DbResult = $DbConnection->query("$Select $From $Where GROUP BY ConfigLanguageID $Having $StrOrderBy $Limit");

     if (!DB::isError($DbResult))
     {
         if ($DbResult->numRows() != 0)
         {
             // Creation of the result array
             $ArrayRecords = array(
                                   "ConfigLanguageID" => array(),
                                   "ConfigLanguageName" => array(),
                                   "ConfigLanguageLang" => array(),
                                   "ConfigLanguageValue" => array()
                                  );

             while($Record = $DbResult->fetchRow(DB_FETCHMODE_ASSOC))
             {
                 $ArrayRecords["ConfigLanguageID"][] = $Record["ConfigLanguageID"];
                 $ArrayRecords["ConfigLanguageName"][] = $Record["ConfigLanguageName"];
                 $ArrayRecords["ConfigLanguageLang"][] = $Record["ConfigLanguageLang"];
                 $ArrayRecords["ConfigLanguageValue"][] = $Record["ConfigLanguageValue"];
             }

             // Return result
             return $ArrayRecords;
         }
     }

     // ERROR
     return array();
 }


/**
 * Get the number of config translation messages filtered by some criterion
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-17
 *
 * @param $DbConnection         DB object              Object of the opened database connection
 * @param $ArrayParams          Mixed array            Contains the criterion used to filter the config
 *                                                     translation messages
 *
 * @return Integer              Number of the config translation messages found, 0 otherwise
 */
 function getNbdbSearchConfigLanguages($DbConnection, $ArrayParams)
 {
     // SQL request to find config translation messages
     $Select = "SELECT cl.ConfigLanguageID";
     $From = "FROM ConfigLanguages cl";
     $Where = "WHERE 1=1";
     $Having = "";

     if (count($ArrayParams) >= 0)
     {
         // <<< ConfigLanguageID field >>>
         if ((array_key_exists("ConfigLanguageID", $ArrayParams)) && (!empty($ArrayParams["ConfigLanguageID"])))
         {
             if (is_array($ArrayParams["ConfigLanguageID"]))
             {
                 $Where .= " AND cl.ConfigLanguageID IN ".constructSQLINString($ArrayParams["ConfigLanguageID"]);
             }
             else
             {
                 $Where .= " AND cl.ConfigLanguageID = ".$ArrayParams["ConfigLanguageID"];
             }
         }

         // <<< ConfigLanguageName field >>>
         if ((array_key_exists("ConfigLanguageName", $ArrayParams)) && (!empty($ArrayParams["ConfigLanguageName"])))
         {
             $Where .= " AND cl.ConfigLanguageName LIKE \"".$ArrayParams["ConfigLanguageName"]."\"";
         }

         // <<< ConfigLanguageLang field >>>
         if ((array_key_exists("ConfigLanguageLang", $ArrayParams)) && (count($ArrayParams["ConfigLanguageLang"]) > 0))
         {
             if (is_array($ArrayParams["ConfigLanguageLang"]))
             {
                 $Where .= " AND cl.ConfigLanguageLang IN ".constructSQLINString($ArrayParams["ConfigLanguageLang"]);
             }
             else
             {
                 $Where .= " AND cl.ConfigLanguageLang LIKE \"".$ArrayParams["ConfigLanguageLang"]."\"";
             }
         }

         // <<< ConfigLanguageValue field >>>
         if ((array_key_exists("ConfigLanguageValue", $ArrayParams)) && (!empty($ArrayParams["ConfigLanguageValue"])))
         {
             $Where .= " AND cl.ConfigLanguageValue LIKE \"".$ArrayParams["ConfigLanguageValue"]."\"";
         }
     }

     // We can launch the SQL request
     $DbResult = $DbConnection->query("$Select $From $Where GROUP BY ConfigLanguageID $Having");
     if (!DB::isError($DbResult))
     {
         return $DbResult->numRows();
     }

     // ERROR
     return 0;
 }
?>
