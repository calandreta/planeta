<?php
/* Copyright (C) 2012 Calandreta Del Pa�s Murethin
 *
 * This file is part of CanteenCalandreta.
 *
 * CanteenCalandreta is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CanteenCalandreta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CanteenCalandreta; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * Common module : library of functions used for create configuration variables from database,
 * before coming from Config.php and for create language variables from database, befor coming
 * from Language files
 *
 * @author Christophe Javouhey
 * @version 3.9
 * @since 2016-11-02
 */


/**
 * Convert an associative array in a string with a usable PHP syntax (for instance, to use the array
 * in the Config.php file)
 *
 * @author Christophe Javouhey
 * @version 1.1
 *     - 2016-11-10 : v1.0.
 *     - 2022-11-04 : v1.1. Taken into account boolean elements
 *
 * @since 2016-11-10
 *
 * @param $ArrayTree        Mixed array       Associative array to convert in PHP syntax
 * @param $Level            Integer           Current level in the tree of the associative array
 * @param $NbSpaces         Integer           Number of white spaces to display in front of an element
 *                                            of the array [0..n]
 * @param $Prefix           String            Prefix to display in front of each element of the array
 *
 * @return String           The associative array in a usable PHP syntax
 */
 function generateArrayTree($ArrayTree, $Level = 1, $NbSpaces = 8, $Prefix = '')
 {
     $sOutput = '';
     $ArrayTreeKeys = array_keys($ArrayTree);
     $iArrayTreeSize = count($ArrayTreeKeys);
     foreach($ArrayTreeKeys as $k => $Key)
     {
         $Element = $ArrayTree[$Key];

         if (is_array($Element))
         {
             $sOutput .= $Prefix.str_repeat("&nbsp;", ($Level - 1) * $NbSpaces);

             if (isInteger($Key))
             {
                 $sOutput .= $Key;
             }
             else
             {
                 $sOutput .= "\"$Key\"";
             }

             $sResult = generateArrayTree($Element, $Level + 1, $NbSpaces, $Prefix);
             if (empty($sResult))
             {
                 $sOutput .= " => Array()";
             }
             else
             {
                 $sOutput .= " => Array(<br />\n";
                 $sOutput .= $sResult;
                 $sOutput .= $Prefix.str_repeat("&nbsp;", (($Level - 1) * $NbSpaces) + strlen("$Key => Array(")).")";
             }

             if ($k < $iArrayTreeSize - 1)
             {
                 // It's not the last element
                 $sOutput .= ",";
             }

             $sOutput .= "<br />\n";
         }
         else
         {
             $sOutput .= $Prefix.str_repeat("&nbsp;", ($Level - 1) * $NbSpaces);

             if (isInteger($Key))
             {
                 $sOutput .= $Key;
             }
             else
             {
                 $sOutput .= "\"$Key\"";
             }

             $sOutput .= " => ";

             // Check the type of value
             if (is_bool($Element))
             {
                 if ($Element)
                 {
                     $sOutput .= "TRUE";
                 }
                 else
                 {
                     $sOutput .= "FALSE";
                 }
             }
             elseif ((isInteger($Element)) || (isFloat($Element)))
             {
                 $sOutput .= $Element;
             }
             elseif (is_string($Element))
             {
                 $sOutput .= "\"$Element\"";
             }
             else
             {
                 $sOutput .= $Element;
             }

             if ($k < $iArrayTreeSize - 1)
             {
                 // It's not the last element
                 $sOutput .= ",";
             }

             $sOutput .= "<br />\n";
         }
     }

     return $sOutput;
 }


/**
 * Give the value well formated and with the right charset
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2016-11-08
 *
 * @param $Value            String       Value to format
 *
 * @return String           The value formatted
 */
 function getXmlNodeValue($Value)
 {
     if ((strToUpper($GLOBALS['CONF_CHARSET']) == 'ISO-8859-1') && (mb_detect_encoding($Value, 'UTF-8') == 'UTF-8'))
     {
         return trim(utf8_decode($Value));
     }
     else
     {
         return trim($Value);
     }
 }


/**
 * Give real child nodes of a given node
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2016-11-08
 *
 * @param $Node                 DOMNode       Node for which we want child nodes
 *
 * @return Array of DOMNode     List of real child nodes
 */
 function getXmlChildren($Node)
 {
     $Children = array();
     if ($Node->hasChildNodes())
     {
         foreach($Node->childNodes as $childNode)
         {
             // We keep only DOMElement nodes
             if ($childNode->nodeType == XML_ELEMENT_NODE)
             {
                 $Children[] = $childNode;
             }
         }
     }

     return $Children;
 }


/**
 * Give keys for an associative array in relation with the full path of a node
 * (to store the value of the node in the associative array)
 *
 * @author Christophe Javouhey
 * @version 1.1
 *     - 2016-11-08 : v1.0.
 *     - 2022-11-03 : v1.1. Before eval(), check if the constant is defined
 *
 * @since 2016-11-08
 *
 * @param $Node                 DOMNode       Node for which we want keys
 * @param $FullPath             Boolean       True to get the complete path of the node,
 *                                            False to keep only some parent nodes
 *
 * @return Array of Strings     List of keys for an associative array
 */
 function getXmlArrayKeys($Node, $FullPath = TRUE)
 {
     $ArrayNodePath = array();
     $bKeepNode = TRUE;

     do
     {
         if ($Node->nodeType == XML_ELEMENT_NODE)
         {
             $bKeepNode = TRUE;
             if (!$FullPath)
             {
                 // We keep only nodes without the "keep" attribute or with "keep" attribute != "0"
                 $KeepAttrValue = $Node->getAttribute('keep');
                 if ($KeepAttrValue === "0")
                 {
                     $bKeepNode = FALSE;
                 }
             }

             if ($bKeepNode)
             {
                 array_unshift($ArrayNodePath, $Node);
             }
         }

         $Node = $Node->parentNode;
     } while (!is_null($Node));

     $ArrayKeys = array();
     $sPath = '';
     foreach($ArrayNodePath as $n => $ParentNode)
     {
         if ($n > 0)
         {
             if (!empty($sPath))
             {
                 $sPath .= "/";
             }

             $sPath .= $ParentNode->nodeName;

             // The key will be the tag name or the value of the "id" attribute if exists
             $AttrIDValue = $ParentNode->getAttribute('id');
             if (empty($AttrIDValue))
             {
                 // No ID : we use the tag name as key
                 $FinalKey = getXmlNodeValue($ParentNode->nodeName);
             }
             else
             {
                 // There is an ID to use as key
                 $FinalKey = getXmlNodeValue($AttrIDValue);
             }

             // Now, we check the type of the key
             $AttrIDTypeValue = $ParentNode->getAttribute('idtype');
             if (empty($AttrIDTypeValue))
             {
                 // No ID type : so, the key is a string or ingeter
                 $ArrayKeys[] = $FinalKey;
             }
             else
             {
                 // The key has a ID type
                 switch(strToLower($AttrIDTypeValue))
                 {
                     case 'const':
                         // The key is a PHP constant declared in the application
                         if (defined($FinalKey))
                         {
                             $iConst = eval("return ".$FinalKey.";");
                             $ArrayKeys[] = $iConst;
                         }
                         break;

                     default:
                         // The key is a string or ingeter
                         $ArrayKeys[] = $FinalKey;
                         break;
                 }
             }
         }
     }

     return $ArrayKeys;
 }


/**
 * Set the value of a node in the right keys of an associative array in relation with
 * the given path (list of keys)
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2016-11-09
 *
 * @param $ArrayResult          Mixed array            Contains the stored values of nodes
 * @param $ArrayPath            Array of Strings       List of keys (path in the associative
 *                                                     array to store the given value),
 * @param $Value                String                 Value to store in the associative array
 */
 function setXMLValueFromPath(&$ArrayResult, $ArrayPath, $Value, $Type = NULL)
 {
     $Dest = &$ArrayResult;
     $FinalKey = array_pop($ArrayPath);
     foreach($ArrayPath as $Key)
     {
         $Dest = &$Dest[$Key];
     }

     if (isset($Dest[$FinalKey]))
     {
         if (is_array($Dest[$FinalKey]))
         {
             // The value is already an array : we add the new value
             $Dest[$FinalKey][] = $Value;
         }
         else
         {
             // The value is a single value : we convert the key in array
             $Dest[$FinalKey] = array($Dest[$FinalKey], $Value);
         }
     }
     else
     {
         // Single value
         switch(strToLower($Type))
         {
             case 'array':
                 if (is_array($Value))
                 {
                     $Dest[$FinalKey] = $Value;
                 }
                 else
                 {
                     $Dest[$FinalKey] = array($Value);
                 }
                 break;

             default:
                 $Dest[$FinalKey] = $Value;
                 break;
         }
     }
 }


/**
 * Convert a node to an associative array (to store the value of the node in
 * the associative array)
 *
 * @author Christophe Javouhey
 * @version 2.0
 *     - 2016-11-08 : v1.0
 *     - 2022-10-27 : v2.0. Taken into account "associative_array" tag and const keys, taken into
 *                    account to interprete "dirname(__FILE__)", $CONF_ROOT_DIRECTORY and $CONF_LANG
 *
 * @since 2016-11-08
 *
 * @param $Node                 DOMNode       Node for which we want keys
 *
 * @return Mixed Array          Node and child nodes converted in an associative array
 *                              in relation with tag names and id
 */
 function getXmlToArray($Node)
 {
     $ArrayResult = array();

     $ArrayXML = getXmlChildren($Node);

     $CurrentIndexXML = 0;
     $ArrayXMLSize = count($ArrayXML);
     $iNbRootNodes = $ArrayXMLSize;

     while ($CurrentIndexXML < $ArrayXMLSize)
     {
         // Get children of the current node
         $Children = getXmlChildren($ArrayXML[$CurrentIndexXML]);
         if (empty($Children))
         {
             // It's a leaf
             $ArrayXML[$CurrentIndexXML]->setAttribute('isLeaf', TRUE);
         }
         else
         {
             $CurrentIndexToInsert = $CurrentIndexXML;
             $ArrayXML[$CurrentIndexXML]->setAttribute('isLeaf', FALSE);

             foreach($Children as $ChildNode)
             {
                 $ArrayXML = array_insertElement($ArrayXML, $ChildNode, $CurrentIndexToInsert);

                 $CurrentIndexToInsert++;
                 $ArrayXMLSize++;
             }
         }

         $CurrentIndexXML++;
     }

     switch(strToLower($ArrayXML[0]->nodeName))
     {
         case 'associative_array':
             $iStartNode = 0;
             if ($iNbRootNodes == 1)
             {
                 // One "associative_array" tag : we jump it
                 $iStartNode = 1;
             }

             // One or several "associative_array" keys
             $ArrayTagsIndexes = array();

             for($i = $iStartNode; $i < $ArrayXMLSize; $i++)
             {
                 $sCurrentTagName = strToLower($ArrayXML[$i]->nodeName);

                 // Compute the depth in the tree
                 $iNbTmpKeys = count($ArrayTagsIndexes);
                 $iDepthPos = 0;

                 foreach($ArrayTagsIndexes as $Tag => $TagValue)
                 {
                     if ($Tag == $sCurrentTagName)
                     {
                         // Found : stop
                         break;
                     }

                     $iDepthPos++;
                 }

                 if ($iDepthPos < $iNbTmpKeys)
                 {
                     // Delete unusefull indexes
                     $ArrayTmpKeys = array_keys($ArrayTagsIndexes);

                     for($k = $iNbTmpKeys - 1; $k > $iDepthPos; $k--)
                     {
                         unset($ArrayTagsIndexes[$ArrayTmpKeys[$k]]);
                     }
                 }

                 if ($ArrayXML[$i]->getAttribute('id') == '')
                 {
                     if (isset($ArrayTagsIndexes[$sCurrentTagName]))
                     {
                         $ArrayTagsIndexes[$sCurrentTagName]++;
                     }
                     else
                     {
                         $ArrayTagsIndexes[$sCurrentTagName] = 0;
                     }
                 }
                 else
                 {
                     // We use the ID as index
                     $sIDTag = getXmlNodeValue($ArrayXML[$i]->getAttribute('id'));
                     if ($ArrayXML[$i]->getAttribute('idtype') == 'const')
                     {
                         // The key is a PHP constant declared in the application
                         if (defined($sIDTag))
                         {
                             $sIDTag = eval("return ".$sIDTag.";");
                         }
                     }

                     $ArrayTagsIndexes[$sCurrentTagName] = $sIDTag;
                 }

                 $Children = getXmlChildren($ArrayXML[$i]);

                 $sXmlNodeValue = getXmlNodeValue($ArrayXML[$i]->nodeValue);
                 if ($ArrayXML[$i]->getAttribute('valuetype') == 'const')
                 {
                     if (defined($sXmlNodeValue))
                     {
                         $sXmlNodeValue = eval("return ".$sXmlNodeValue.";");
                     }
                 }
                 elseif (stripos(substr($sXmlNodeValue, 0, strlen("dirname(__FILE__)")), "dirname(__FILE__)") !== FALSE)
                 {
                     // Interprete "dirname(__FILE__)" string value
                     $sXmlNodeValue = str_replace(
                                                  array("dirname(__FILE__)", '."/', '"'),
                                                  array(dirname(__FILE__), '/', ''),
                                                  $sXmlNodeValue
                                                 );
                 }
                 elseif (stripos(substr($sXmlNodeValue, 0, strlen('$CONF_ROOT_DIRECTORY')), '$CONF_ROOT_DIRECTORY') !== FALSE)
                 {
                     // Interprete "$CONF_ROOT_DIRECTORY" string value
                     $sXmlNodeValue = str_replace(
                                                  array('$CONF_ROOT_DIRECTORY', '."', '"'),
                                                  array($GLOBALS['CONF_ROOT_DIRECTORY'], '', ''),
                                                  $sXmlNodeValue
                                                 );
                 }
                 elseif (stripos(substr($sXmlNodeValue, 0, strlen('$CONF_LANG')), '$CONF_LANG') !== FALSE)
                 {
                     // Interprete "$CONF_LANG" string value
                     $sXmlNodeValue = str_replace(
                                                  array('$CONF_LANG', '."', '"'),
                                                  array($GLOBALS['CONF_LANG'], '', ''),
                                                  $sXmlNodeValue
                                                 );
                 }

                 // Compute the path in the array to store the value
                 $iNbTmpKeys = count($ArrayTagsIndexes);
                 $ArrayTmpKeys = array_keys($ArrayTagsIndexes);

                 switch($iNbTmpKeys)
                 {
                     case 1:
                         if (count($Children) == 0)
                         {
                             $ArrayResult[$ArrayTagsIndexes[$ArrayTmpKeys[0]]] = $sXmlNodeValue;
                         }
                         else
                         {
                             $ArrayResult[$ArrayTagsIndexes[$ArrayTmpKeys[0]]] = array();
                         }
                         break;

                     case 2:
                         if (count($Children) == 0)
                         {
                             $ArrayResult[$ArrayTagsIndexes[$ArrayTmpKeys[0]]][$ArrayTagsIndexes[$ArrayTmpKeys[1]]] = $sXmlNodeValue;
                         }
                         else
                         {
                             $ArrayResult[$ArrayTagsIndexes[$ArrayTmpKeys[0]]][$ArrayTagsIndexes[$ArrayTmpKeys[1]]] = array();
                         }
                         break;

                     case 3:
                         if (count($Children) == 0)
                         {
                             $ArrayResult[$ArrayTagsIndexes[$ArrayTmpKeys[0]]][$ArrayTagsIndexes[$ArrayTmpKeys[1]]][$ArrayTagsIndexes[$ArrayTmpKeys[2]]] = $sXmlNodeValue;
                         }
                         else
                         {
                             $ArrayResult[$ArrayTagsIndexes[$ArrayTmpKeys[0]]][$ArrayTagsIndexes[$ArrayTmpKeys[1]]][$ArrayTagsIndexes[$ArrayTmpKeys[2]]] = array();
                         }
                         break;

                     case 4:
                         if (count($Children) == 0)
                         {
                             $ArrayResult[$ArrayTagsIndexes[$ArrayTmpKeys[0]]][$ArrayTagsIndexes[$ArrayTmpKeys[1]]][$ArrayTagsIndexes[$ArrayTmpKeys[2]]][$ArrayTagsIndexes[$ArrayTmpKeys[3]]] = $sXmlNodeValue;
                         }
                         else
                         {
                             $ArrayResult[$ArrayTagsIndexes[$ArrayTmpKeys[0]]][$ArrayTagsIndexes[$ArrayTmpKeys[1]]][$ArrayTagsIndexes[$ArrayTmpKeys[2]]][$ArrayTagsIndexes[$ArrayTmpKeys[3]]] = array();
                         }
                         break;

                     case 5:
                         if (count($Children) == 0)
                         {
                             $ArrayResult[$ArrayTagsIndexes[$ArrayTmpKeys[0]]][$ArrayTagsIndexes[$ArrayTmpKeys[1]]][$ArrayTagsIndexes[$ArrayTmpKeys[2]]][$ArrayTagsIndexes[$ArrayTmpKeys[3]]][$ArrayTagsIndexes[$ArrayTmpKeys[4]]] = $sXmlNodeValue;
                         }
                         break;
                 }
             }

             unset($ArrayTagsIndexes);
             break;

         default:
             // Use tagenames as keys in the associative array
             foreach($ArrayXML as $n => $CurrentNode)
             {
                 // We store value of each leaf node in the right keys of the associative array
                 if ($CurrentNode->getAttribute('isLeaf'))
                 {
                     // Get keys in relation with the path of the node in the XML tree
                     $ArrayPath = getXmlArrayKeys($CurrentNode, FALSE);

                     $NodeValue = getXmlNodeValue($CurrentNode->nodeValue);

                     $ValueType = NULL;
                     if (!is_null($CurrentNode->parentNode))
                     {
                         // Get the type of value (specified on the parent node)
                         $ValueType = getXmlNodeValue($CurrentNode->parentNode->getAttribute('type'));
                         if (empty($ValueType))
                         {
                             // In the case of a parent node with a type of value but without child nodes
                             $ValueType = getXmlNodeValue($CurrentNode->getAttribute('type'));

                             // To specify a default value in relation with the type of value
                             // if the node has an empty value
                             switch(strToLower($ValueType))
                             {
                                 case 'array':
                                     $NodeValue = array();
                                     break;
                             }
                         }
                     }

                     // Store its value in the keys of the associative array
                     setXMLValueFromPath($ArrayResult, $ArrayPath, $NodeValue, $ValueType);
                 }
             }
             break;
     }

     return $ArrayResult;
 }


/**
 * Load config parameters from database in config variables coming from Config.php
 *
 * @author Christophe Javouhey
 * @version 2.2
 *     - 2020-02-18 : v1.1. Taken into account CONF_NURSERY_OTHER_TIMESLOTS variable
 *     - 2022-06-06 : v2.0. Taken into account new formats of CONF_NURSERY_PRICES variable
 *     - 2022-07-11 : v2.1. Taken into account new formats of CONF_CANTEEN_PRICES variable
 *     - 2022-10-14 : v2.2. Auto-declare CONF_xxxxxx variables in global, taken into account
 *                    string, boolean, number, const types, check if constants are defined before eval(),
 *                    taken into account CONF_ACCESS_APPL_PAGES and ACCESS_CONTEXTUALMENUS variables,
 *                    remove strToLower() on ConfigParameterType
 *
 * @since 2016-11-02
 *
 * @param $DbConnection             DB object              Object of the opened database connection
 * @param $ArrayParams              Mixed array            Contains names of config parameters to load.
 *                                                         If empty, load all config variables
 */
 function loadDbConfigParameters($DbConnection, $ArrayParams = array())
 {
     // These config variables come from Config.php
     if (empty($ArrayParams))
     {
         // We load all config variables
         $sPrefix = 'CONF_';
         $iNbChars = strlen($sPrefix);
         foreach($GLOBALS as $VarName => $VarValue)
         {
             if (substr($VarName, 0, $iNbChars) == $sPrefix)
             {
                 $ArrayParams[] = $VarName;
             }
         }

         if (isset($GLOBALS['ACCESS_CONTEXTUALMENUS']))
         {
             $ArrayParams[] = 'ACCESS_CONTEXTUALMENUS';
         }
     }

     // Get all config variables from DB
     $ArrayDbConfParams = array();
     $ArrayConfigParameters = dbSearchConfigParameters($DbConnection, array(), "ConfigParameterID", 1, 0);

     if ((isset($ArrayConfigParameters['ConfigParameterID'])) && (!empty($ArrayConfigParameters['ConfigParameterID'])))
     {
         foreach($ArrayConfigParameters['ConfigParameterName'] as $c => $CurrentParamName)
         {
             $ArrayDbConfParams[$CurrentParamName] = array(
                                                           'ConfigParameterID' => $ArrayConfigParameters['ConfigParameterID'][$c],
                                                           'ConfigParameterName' => $CurrentParamName,
                                                           'ConfigParameterType' => $ArrayConfigParameters['ConfigParameterType'][$c],
                                                           'ConfigParameterValue' => $ArrayConfigParameters['ConfigParameterValue'][$c]
                                                          );
         }
     }

     unset($ArrayConfigParameters);

     // Set new value to config variables
     foreach($ArrayParams as $p => $ParamName)
     {
         $ParamName = strToUpper($ParamName);

         // Declare this global variable in this function
         if (isset($GLOBALS[$ParamName]))
         {
             global ${$ParamName};
         }

         // We check if the config variable comes from DB
         if (isset($ArrayDbConfParams[$ParamName]))
         {
             $RecordParamValue = $ArrayDbConfParams[$ParamName];

             switch($ParamName)
             {
                 case 'ACCESS_CONTEXTUALMENUS':
                     // Reinit
                     $ACCESS_CONTEXTUALMENUS = array();

                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument;
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $ContextualMenuAccess = $XmlParser->getElementsByTagName('contextualmenu-access');
                             foreach($ContextualMenuAccess as $CurrCMAccess)
                             {
                                 // Get the concerned support member state ID
                                 $iSupportMemberStateID = $CurrCMAccess->getAttribute('id');

                                 // Get sections of the contextual menu for the support member state ID
                                 $ContextualMenuSections = $CurrCMAccess->getElementsByTagName('section');
                                 foreach($ContextualMenuSections as $CurrCMSection)
                                 {
                                     // Get the section name
                                     $sSectionName = $CurrCMSection->getAttribute('id');

                                     $ACCESS_CONTEXTUALMENUS[$iSupportMemberStateID][$sSectionName] = array();

                                     // Get items access of the section
                                     $ContextualMenuItems = $CurrCMSection->getElementsByTagName('item');
                                     foreach($ContextualMenuItems as $CurrCMItem)
                                     {
                                         // Get the ID of the contextual menu item
                                         $iItemID = $CurrCMItem->getAttribute('id');
                                         if (defined($iItemID))
                                         {
                                             $iItemID = eval("return ".$iItemID.";");
                                         }

                                         // Get the value of the contextual menu item (0, 1, FALSE, TRUE)
                                         $bValue = FALSE;
                                         $sItemValue = strToUpper(getXmlNodeValue($CurrCMItem->nodeValue));
                                         if (($sItemValue == '1') || ($sItemValue == 'TRUE'))
                                         {
                                             $bValue = TRUE;
                                         }

                                         $ACCESS_CONTEXTUALMENUS[$iSupportMemberStateID][$sSectionName][$iItemID] = $bValue;
                                     }
                                 }
                             }

                             unset($XmlParser, $ContextualMenuAccess, $ContextualMenuSections, $ContextualMenuItems);
                             break;
                     }
                     break;

                 case 'CONF_ACCESS_APPL_PAGES':
                     // Reinit
                     $CONF_ACCESS_APPL_PAGES = array();

                     // Load credentials for each part of the application
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument;
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $Credentials = $XmlParser->getElementsByTagName('credential');
                             foreach($Credentials as $CurrCredential)
                             {
                                 // Get the concerned function of the application bu credentials
                                 $iFunctionID = $CurrCredential->getAttribute('id');
                                 if (defined($iFunctionID))
                                 {
                                     $iFunctionID = eval("return ".$iFunctionID.";");
                                 }

                                 $Actions = $CurrCredential->getElementsByTagName('action');
                                 foreach($Actions as $CurrAction)
                                 {
                                     // Get the concerned action
                                     $iActionID = $CurrAction->getAttribute('id');
                                     if (defined($iActionID))
                                     {
                                         $iActionID = eval("return ".$iActionID.";");
                                     }

                                     // Get the concerned support member states ID
                                     $ArrayAccess = array();
                                     $sAccess = getXmlNodeValue($CurrAction->nodeValue);
                                     if (!empty($sAccess))
                                     {
                                         $ArrayAccess = explode(',', $sAccess);
                                         foreach($ArrayAccess as $a => $CurrAccess)
                                         {
                                             $ArrayAccess[$a] = trim($CurrAccess);
                                         }
                                     }

                                     $CONF_ACCESS_APPL_PAGES[$iFunctionID][$iActionID] = $ArrayAccess;

                                     unset($ArrayAccess);
                                 }
                             }

                             unset($XmlParser, $Credentials, $Actions);
                             break;
                     }
                     break;

                 case 'CONF_SCHOOL_YEAR_START_DATES':
                     // Reinit
                     $CONF_SCHOOL_YEAR_START_DATES = array();

                     // Load start date of each school year
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument;
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $SchoolYears = $XmlParser->getElementsByTagName('school-year');
                             foreach($SchoolYears as $SchoolYear)
                             {
                                 $CONF_SCHOOL_YEAR_START_DATES[$SchoolYear->getAttribute('id')] = getXmlNodeValue($SchoolYear->nodeValue);
                             }

                             unset($XmlParser, $SchoolYears);
                             break;
                     }
                     break;

                 case 'CONF_CLASSROOMS':
                     // Reinit
                     $CONF_CLASSROOMS = array();

                     // Load classrooms for each school year
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument;
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $SchoolYears = $XmlParser->getElementsByTagName('school-year');
                             foreach($SchoolYears as $SchoolYear)
                             {
                                 $Classrooms = $SchoolYear->getElementsByTagName('classroom');
                                 foreach($Classrooms as $Classroom)
                                 {
                                     $CONF_CLASSROOMS[$SchoolYear->getAttribute('id')][] = getXmlNodeValue($Classroom->nodeValue);
                                 }
                             }

                             unset($XmlParser, $SchoolYears, $Classrooms);
                             break;
                     }
                     break;

                 case 'CONF_CONTRIBUTIONS_ANNUAL_AMOUNTS':
                     // Reinit
                     $CONF_CONTRIBUTIONS_ANNUAL_AMOUNTS = array();

                     // Load annual contributions amounts for each school year
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument;
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $SchoolYears = $XmlParser->getElementsByTagName('school-year');
                             foreach($SchoolYears as $SchoolYear)
                             {
                                 $SY_ID = $SchoolYear->getAttribute('id');
                                 $Amounts = $SchoolYear->getElementsByTagName('amount');
                                 foreach($Amounts as $Amount)
                                 {
                                     $CONF_CONTRIBUTIONS_ANNUAL_AMOUNTS[$SY_ID][$Amount->getAttribute('nbvotes')] = getXmlNodeValue($Amount->nodeValue);
                                 }
                             }

                             unset($XmlParser, $SchoolYears, $Amounts);
                             break;
                     }
                     break;

                 case 'CONF_CONTRIBUTIONS_MONTHLY_AMOUNTS':
                     // Reinit
                     $CONF_CONTRIBUTIONS_MONTHLY_AMOUNTS = array();

                     // Load monthly contributions amounts for each school year
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument;
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $SchoolYears = $XmlParser->getElementsByTagName('school-year');
                             foreach($SchoolYears as $SchoolYear)
                             {
                                 $SY_ID = $SchoolYear->getAttribute('id');
                                 if ($SchoolYear->hasChildNodes())
                                 {
                                     // Get contribution modes for this school year
                                     foreach($SchoolYear->childNodes as $ChildNode)
                                     {
                                         if ($ChildNode->nodeType != XML_TEXT_NODE)
                                         {
                                             if ($ChildNode->attributes->length > 0)
                                             {
                                                 // This attribute is a constant : contribution modes !!!
                                                 $iMode = getXmlNodeValue($ChildNode->attributes->item(0)->nodeValue);
                                                 if (defined($iMode))
                                                 {
                                                     $iMode = eval("return ".$iMode.";");
                                                 }

                                                 if ($ChildNode->hasChildNodes())
                                                 {
                                                     // Get amounts in relation with nb of children for this contribution mode
                                                     foreach($ChildNode->childNodes as $SubChildNode)
                                                     {
                                                         if ($SubChildNode->nodeType != XML_TEXT_NODE)
                                                         {
                                                             if ($SubChildNode->attributes->length > 0)
                                                             {
                                                                 $iNbChildren = getXmlNodeValue($SubChildNode->attributes->item(0)->nodeValue);
                                                                 $CONF_CONTRIBUTIONS_MONTHLY_AMOUNTS[$SY_ID][$iMode][$iNbChildren] = getXmlNodeValue($SubChildNode->nodeValue);
                                                             }
                                                             else
                                                             {
                                                                 $CONF_CONTRIBUTIONS_MONTHLY_AMOUNTS[$SY_ID][$iMode][] = getXmlNodeValue($SubChildNode->nodeValue);
                                                             }
                                                         }
                                                     }
                                                 }
                                                 else
                                                 {
                                                     if ($ChildNode->nodeType != XML_TEXT_NODE)
                                                     {
                                                         $CONF_CONTRIBUTIONS_MONTHLY_AMOUNTS[$SY_ID][$iMode][] = getXmlNodeValue($ChildNode->nodeValue);
                                                     }
                                                 }
                                             }
                                             else
                                             {
                                                 $CONF_CONTRIBUTIONS_MONTHLY_AMOUNTS[$SchoolYear->getAttribute('id')][] = getXmlNodeValue($ChildNode->nodeValue);
                                             }
                                         }
                                     }
                                 }
                                 else
                                 {
                                     // No value
                                     $CONF_CONTRIBUTIONS_MONTHLY_AMOUNTS[$SchoolYear->getAttribute('id')] = array();
                                 }
                             }

                             unset($XmlParser, $SchoolYears);
                             break;
                     }
                     break;

                 case 'CONF_CANTEEN_PRICES':
                     // Reinit
                     $CONF_CANTEEN_PRICES = array();

                     // Load canteen prices for each school year
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument;
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $SchoolYears = $XmlParser->getElementsByTagName('school-year');
                             foreach($SchoolYears as $SchoolYear)
                             {
                                 $SY_ID = $SchoolYear->getAttribute('id');
                                 if ($SchoolYear->hasChildNodes())
                                 {
                                     foreach($SchoolYear->childNodes as $ChildNode)
                                     {
                                         if ($ChildNode->nodeType != XML_TEXT_NODE)
                                         {
                                             switch($ChildNode->nodeName)
                                             {
                                                 case 'price-child-grade':
                                                     // We get child grades for which the prices are defined
                                                     $sPriceGrades = str_replace(array(' '), array(''), getXmlNodeValue($ChildNode->getAttribute('grades')));
                                                     if (!empty($sPriceGrades))
                                                     {
                                                         $ArrayPriceGrades = explode(',', $sPriceGrades);
                                                         $ArrayPrices = array();

                                                         foreach($ChildNode->childNodes as $GradeChildNode)
                                                         {
                                                             if ($GradeChildNode->nodeType != XML_TEXT_NODE)
                                                             {
                                                                 switch($GradeChildNode->nodeName)
                                                                 {
                                                                     case 'monthly-contribution':
                                                                         // This attribute is a constant : contribution modes !!!
                                                                         $iContribMode = getXmlNodeValue($GradeChildNode->getAttribute('mode'));
                                                                         if (defined($iContribMode))
                                                                         {
                                                                             $iContribMode = eval("return ".$iContribMode.";");
                                                                         }

                                                                         $ArrayPrices['C_'.$iContribMode] = array();
                                                                         foreach($GradeChildNode->childNodes as $ContribChildNode)
                                                                         {
                                                                             if ($ContribChildNode->nodeType != XML_TEXT_NODE)
                                                                             {
                                                                                 switch($ContribChildNode->nodeName)
                                                                                 {
                                                                                     case 'lunch-price':
                                                                                         $ArrayPrices['C_'.$iContribMode][0] = getXmlNodeValue($ContribChildNode->nodeValue);
                                                                                         break;

                                                                                     case 'nursery-price':
                                                                                         $ArrayPrices['C_'.$iContribMode][1] = getXmlNodeValue($ContribChildNode->nodeValue);
                                                                                         break;
                                                                                 }
                                                                             }
                                                                         }
                                                                         break;

                                                                     case 'lunch-price':
                                                                         $ArrayPrices[0] = getXmlNodeValue($GradeChildNode->nodeValue);
                                                                         break;

                                                                     case 'nursery-price':
                                                                         $ArrayPrices[1] = getXmlNodeValue($GradeChildNode->nodeValue);
                                                                         break;
                                                                 }
                                                             }
                                                         }

                                                         // We duplicate the prices rules for each child grade
                                                         foreach($ArrayPriceGrades as $g => $CurrentGrade)
                                                         {
                                                             $iGradePos = array_search($CurrentGrade, $GLOBALS['CONF_GRADES']);
                                                             if ($iGradePos !== FALSE)
                                                             {
                                                                 $CONF_CANTEEN_PRICES[$SY_ID]['G_'.$iGradePos] = $ArrayPrices;
                                                             }
                                                         }

                                                         unset($ArrayPrices, $ArrayPriceGrades);
                                                     }
                                                     break;

                                                 case 'lunch-price':
                                                     $CONF_CANTEEN_PRICES[$SY_ID][0] = getXmlNodeValue($ChildNode->nodeValue);
                                                     break;

                                                 case 'nursery-price':
                                                     $CONF_CANTEEN_PRICES[$SY_ID][1] = getXmlNodeValue($ChildNode->nodeValue);
                                                     break;
                                             }
                                         }
                                     }
                                 }
                                 else
                                 {
                                     // No value
                                     $CONF_CANTEEN_PRICES[$SchoolYear->getAttribute('id')] = array();
                                 }
                             }

                             unset($XmlParser, $SchoolYears);
                             break;
                     }
                     break;

                 case 'CONF_NURSERY_OTHER_TIMESLOTS':
                     // R�init
                     $CONF_NURSERY_OTHER_TIMESLOTS = array();

                     // Load data about other nursery timeslots for each school year
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument;
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $SchoolYears = $XmlParser->getElementsByTagName('school-year');
                             foreach($SchoolYears as $SchoolYear)
                             {
                                 $SY_ID = $SchoolYear->getAttribute('id');
                                 if ($SchoolYear->hasChildNodes())
                                 {
                                     foreach($SchoolYear->childNodes as $ChildNode)
                                     {
                                         if ($ChildNode->nodeType != XML_TEXT_NODE)
                                         {
                                             $TimeslotID = $ChildNode->getAttribute('id');
                                             if (!empty($TimeslotID))
                                             {
                                                 // We remove whitespaces between TRUE/FALSE values
                                                 // TRUE and FALSE are converted into 1 and 0 because there are strings and not boolean valuses
                                                 $sWeekDays = str_replace(array(' ', 'FALSE', 'TRUE'), array('', '0', '1'), getXmlNodeValue($ChildNode->nodeValue));

                                                 // We get info about links between nursery and canteen
                                                 $bCheckCanteen = 0;
                                                 if ($ChildNode->getAttribute('check-canteen') == '1')
                                                 {
                                                     $bCheckCanteen = 1;
                                                 }

                                                 $bLinkedToCanteen = 0;
                                                 if ($ChildNode->getAttribute('linked-to-canteen') == '1')
                                                 {
                                                     $bLinkedToCanteen = 1;
                                                 }

                                                 $ArrayCheckOtherTimeslots = array();
                                                 if ($ChildNode->getAttribute('check-nursery') != '')
                                                 {
                                                     $ArrayCheckOtherTimeslots = explode(',', $ChildNode->getAttribute('check-nursery'));
                                                     if (!empty($ArrayCheckOtherTimeslots))
                                                     {
                                                         foreach($ArrayCheckOtherTimeslots as $ots => $CurrentOTS)
                                                         {
                                                             $ArrayCheckOtherTimeslots[$ots] = trim($CurrentOTS);
                                                         }
                                                     }
                                                 }

                                                 $CONF_NURSERY_OTHER_TIMESLOTS[$SY_ID][$TimeslotID] = array(
                                                                                                            'Label' => trim($ChildNode->getAttribute('label')),
                                                                                                            'WeekDays' => explode(',', $sWeekDays),
                                                                                                            'CheckCanteen' => $bCheckCanteen,
                                                                                                            'LinkedToCanteen' => $bLinkedToCanteen,
                                                                                                            'CheckNursery' => $ArrayCheckOtherTimeslots
                                                                                                           );
                                             }
                                         }
                                     }
                                 }
                                 else
                                 {
                                     // No value
                                     $CONF_NURSERY_OTHER_TIMESLOTS[$SchoolYear->getAttribute('id')] = array();
                                 }
                             }

                             unset($XmlParser, $SchoolYears);
                             break;
                     }
                     break;

                 case 'CONF_NURSERY_PRICES':
                     // Reinit
                     $CONF_NURSERY_PRICES = array();

                     // Load nursery prices for each school year
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument;
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $SchoolYears = $XmlParser->getElementsByTagName('school-year');
                             foreach($SchoolYears as $SchoolYear)
                             {
                                 $SY_ID = $SchoolYear->getAttribute('id');
                                 if ($SchoolYear->hasChildNodes())
                                 {
                                     foreach($SchoolYear->childNodes as $ChildNode)
                                     {
                                         if ($ChildNode->nodeType != XML_TEXT_NODE)
                                         {
                                             switch($ChildNode->nodeName)
                                             {
                                                 case 'price-mode':
                                                     // We get the mode name and the type (annual or monthly)
                                                     $sPriceMode = getXmlNodeValue($ChildNode->getAttribute('mode'));
                                                     $sTypeMode = getXmlNodeValue($ChildNode->getAttribute('type'));
                                                     if (!empty($sPriceMode))
                                                     {
                                                         if ((empty($sTypeMode)) || (!in_array($sTypeMode, array('M', 'A'))))
                                                         {
                                                             // By default, monthly mode
                                                             $sTypeMode = 'M';
                                                         }

                                                         $sPriceMode .= '_'.$sTypeMode;

                                                         $CONF_NURSERY_PRICES[$SY_ID][$sPriceMode] = array();
                                                         foreach($ChildNode->childNodes as $GradeNode)
                                                         {
                                                             if (($GradeNode->nodeType != XML_TEXT_NODE)
                                                                 && ($GradeNode->nodeName == 'price-child-grade'))
                                                             {
                                                                 // We get child grades for which the prices are defined
                                                                 $sPriceGrades = str_replace(array(' '), array(''), getXmlNodeValue($GradeNode->getAttribute('grades')));
                                                                 if (!empty($sPriceGrades))
                                                                 {
                                                                     $ArrayPriceGrades = explode(',', $sPriceGrades);
                                                                     $ArrayPrices = array();

                                                                     foreach($GradeNode->childNodes as $GradeChildNode)
                                                                     {
                                                                         if ($GradeChildNode->nodeType != XML_TEXT_NODE)
                                                                         {
                                                                             switch($GradeChildNode->nodeName)
                                                                             {
                                                                                 case 'monthly-contribution':
                                                                                     // This attribute is a constant : contribution modes !!!
                                                                                     $iContribMode = getXmlNodeValue($GradeChildNode->getAttribute('mode'));
                                                                                     if (defined($iContribMode))
                                                                                     {
                                                                                         $iContribMode = eval("return ".$iContribMode.";");
                                                                                     }

                                                                                     $ArrayPrices['C_'.$iContribMode] = array();
                                                                                     foreach($GradeChildNode->childNodes as $ContribChildNode)
                                                                                     {
                                                                                         if ($ContribChildNode->nodeType != XML_TEXT_NODE)
                                                                                         {
                                                                                             switch($ContribChildNode->nodeName)
                                                                                             {
                                                                                                 case 'other-timeslots':
                                                                                                     // We load prices of other timeslots (not AM or PM timeslots)
                                                                                                     $ArrayPrices['C_'.$iContribMode]['OtherTimeslots'] = array();
                                                                                                     foreach($ContribChildNode->childNodes as $OtherTimeslotNode)
                                                                                                     {
                                                                                                         if ($OtherTimeslotNode->nodeType != XML_TEXT_NODE)
                                                                                                         {
                                                                                                             $TimeslotID = $OtherTimeslotNode->getAttribute('id');
                                                                                                             if (!empty($TimeslotID))
                                                                                                             {
                                                                                                                 $ArrayPrices['C_'.$iContribMode]['OtherTimeslots'][$TimeslotID] = getXmlNodeValue($OtherTimeslotNode->nodeValue);
                                                                                                             }
                                                                                                         }
                                                                                                     }
                                                                                                     break;

                                                                                                 case 'am-pm-nursery-price':
                                                                                                     // Price for both (AM and PM)
                                                                                                     $ArrayPrices['C_'.$iContribMode]['0_1'] = getXmlNodeValue($ContribChildNode->nodeValue);
                                                                                                     break;

                                                                                                 case 'am-nursery-price':
                                                                                                     $ArrayPrices['C_'.$iContribMode][0] = getXmlNodeValue($ContribChildNode->nodeValue);
                                                                                                     break;

                                                                                                 case 'pm-nursery-price':
                                                                                                     $ArrayPrices['C_'.$iContribMode][1] = getXmlNodeValue($ContribChildNode->nodeValue);
                                                                                                     break;
                                                                                             }
                                                                                         }
                                                                                     }
                                                                                     break;

                                                                                 case 'other-timeslots':
                                                                                     // We load prices of other timeslots (not AM or PM timeslots)
                                                                                     $ArrayPrices['OtherTimeslots'] = array();
                                                                                     foreach($GradeChildNode->childNodes as $OtherTimeslotNode)
                                                                                     {
                                                                                         if ($OtherTimeslotNode->nodeType != XML_TEXT_NODE)
                                                                                         {
                                                                                             $TimeslotID = $OtherTimeslotNode->getAttribute('id');
                                                                                             if (!empty($TimeslotID))
                                                                                             {
                                                                                                 $ArrayPrices['OtherTimeslots'][$TimeslotID] = getXmlNodeValue($OtherTimeslotNode->nodeValue);
                                                                                             }
                                                                                         }
                                                                                     }
                                                                                     break;

                                                                                 case 'am-nursery-price':
                                                                                     $ArrayPrices[0] = getXmlNodeValue($GradeChildNode->nodeValue);
                                                                                     break;

                                                                                 case 'pm-nursery-price':
                                                                                     $ArrayPrices[1] = getXmlNodeValue($GradeChildNode->nodeValue);
                                                                                     break;
                                                                             }
                                                                         }
                                                                     }

                                                                     // We duplicate the prices rules for each child grade
                                                                     foreach($ArrayPriceGrades as $g => $CurrentGrade)
                                                                     {
                                                                         $iGradePos = array_search($CurrentGrade, $GLOBALS['CONF_GRADES']);
                                                                         if ($iGradePos !== FALSE)
                                                                         {
                                                                             $CONF_NURSERY_PRICES[$SY_ID][$sPriceMode]['G_'.$iGradePos] = $ArrayPrices;
                                                                         }
                                                                     }

                                                                     unset($ArrayPrices, $ArrayPriceGrades);
                                                                 }
                                                             }
                                                         }
                                                     }
                                                     break;

                                                 case 'other-timeslots':
                                                     // We check if there are other timeslots prices
                                                     // We load prices of other timeslots (not AM or PM timeslots)
                                                     $CONF_NURSERY_PRICES[$SY_ID]['OtherTimeslots'] = array();
                                                     foreach($ChildNode->childNodes as $OtherTimeslotNode)
                                                     {
                                                         if ($OtherTimeslotNode->nodeType != XML_TEXT_NODE)
                                                         {
                                                             $TimeslotID = $OtherTimeslotNode->getAttribute('id');
                                                             if (!empty($TimeslotID))
                                                             {
                                                                 $CONF_NURSERY_PRICES[$SY_ID]['OtherTimeslots'][$TimeslotID] = getXmlNodeValue($OtherTimeslotNode->nodeValue);
                                                             }
                                                         }
                                                     }
                                                     break;

                                                 case 'am-nursery-price':
                                                     $CONF_NURSERY_PRICES[$SY_ID][0] = getXmlNodeValue($ChildNode->nodeValue);
                                                     break;

                                                 case 'pm-nursery-price':
                                                     $CONF_NURSERY_PRICES[$SY_ID][1] = getXmlNodeValue($ChildNode->nodeValue);
                                                     break;
                                             }
                                         }
                                     }
                                 }
                                 else
                                 {
                                     // No value
                                     $CONF_NURSERY_PRICES[$SY_ID] = array();
                                 }
                             }

                             unset($XmlParser, $SchoolYears);
                             break;
                     }
                     break;

                 case 'CONF_NURSERY_DELAYS_PRICES':
                     // Reinit
                     $CONF_NURSERY_DELAYS_PRICES = array();

                     // Load nursery delays prices for each school year
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument;
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $SchoolYears = $XmlParser->getElementsByTagName('school-year');
                             foreach($SchoolYears as $SchoolYear)
                             {
                                 $SY_ID = $SchoolYear->getAttribute('id');
                                 if ($SchoolYear->hasChildNodes())
                                 {
                                     foreach($SchoolYear->childNodes as $ChildNode)
                                     {
                                         if ($ChildNode->nodeType != XML_TEXT_NODE)
                                         {
                                             if ($ChildNode->attributes->length > 0)
                                             {
                                                 $CONF_NURSERY_DELAYS_PRICES[$SY_ID][getXmlNodeValue($ChildNode->attributes->item(0)->nodeValue)] = getXmlNodeValue($ChildNode->nodeValue);
                                             }
                                             else
                                             {
                                                 $CONF_NURSERY_DELAYS_PRICES[$SY_ID][] = getXmlNodeValue($ChildNode->nodeValue);
                                             }
                                         }
                                     }
                                 }
                                 else
                                 {
                                     // No value
                                     $CONF_NURSERY_DELAYS_PRICES[$SY_ID] = array();
                                 }
                             }

                             unset($XmlParser, $SchoolYears);
                             break;
                     }
                     break;

                 case 'CONF_DONATION_TAX_RECEIPT_PARAMETERS':
                     // Reinit
                     $CONF_DONATION_TAX_RECEIPT_PARAMETERS = array();

                     // Load donation tax receipt parameters for each school year
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument();
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);

                             $Years = $XmlParser->getElementsByTagName('year');
                             foreach($Years as $Year)
                             {
                                 $Y_ID = $Year->getAttribute('id');
                                 if ($Year->hasChildNodes())
                                 {
                                     foreach($Year->childNodes as $ChildNode)
                                     {
                                         if ($ChildNode->nodeType == XML_ELEMENT_NODE)
                                         {
                                             if (in_array(getXmlNodeValue($ChildNode->nodeName), array('Template', 'Language', 'Unit')))
                                             {
                                                 // Single parameters for the year
                                                 $iParam = getXmlNodeValue($ChildNode->nodeName);
                                                 if (defined($iParam))
                                                 {
                                                     $iParam = eval("return ".$iParam.";");
                                                 }

                                                 $CONF_DONATION_TAX_RECEIPT_PARAMETERS[$Y_ID][$iParam] = getXmlNodeValue($ChildNode->nodeValue);
                                             }
                                             elseif ($ChildNode->nodeName == 'pages')
                                             {
                                                 // Parameters for pages of the tax receipt of the year
                                                 if ($ChildNode->hasChildNodes())
                                                 {
                                                     foreach($ChildNode->childNodes as $PageNode)
                                                     {
                                                         if ($PageNode->nodeType == XML_ELEMENT_NODE)
                                                         {
                                                             // Id of the page
                                                             $PageID = $PageNode->getAttribute('id');
                                                             $CONF_DONATION_TAX_RECEIPT_PARAMETERS[$Y_ID][Page][$PageID] = array();

                                                             // We get parameters of the page
                                                             $PageParts = $PageNode->getElementsByTagName('part');
                                                             foreach($PageParts as $PartNode)
                                                             {
                                                                 // Parts are Recipient, Donator... constants
                                                                 $iPart = $PartNode->getAttribute('id');
                                                                 if (defined($iPart))
                                                                 {
                                                                     $iPart = eval("return ".$iPart.";");
                                                                 }

                                                                 $CONF_DONATION_TAX_RECEIPT_PARAMETERS[$Y_ID][Page][$PageID][$iPart] = array();

                                                                 // We get parameters of each field
                                                                 $PartFields = $PartNode->getElementsByTagName('field');
                                                                 foreach($PartFields as $FieldNode)
                                                                 {
                                                                     $FieldID = getXmlNodeValue($FieldNode->getAttribute('id'));
                                                                     if ($FieldNode->hasChildNodes())
                                                                     {
                                                                         foreach($FieldNode->childNodes as $ParamFieldNode)
                                                                         {
                                                                             if ($ParamFieldNode->nodeType == XML_ELEMENT_NODE)
                                                                             {
                                                                                 // We check if the field is a list
                                                                                 if (getXmlNodeValue($ParamFieldNode->nodeName) == 'list')
                                                                                 {
                                                                                     // Get items of the list
                                                                                     $FieldItems = $ParamFieldNode->getElementsByTagName('item');
                                                                                     foreach($FieldItems as $ItemNode)
                                                                                     {
                                                                                         if ($ItemNode->nodeType == XML_ELEMENT_NODE)
                                                                                         {
                                                                                             $ItemID = getXmlNodeValue($ItemNode->getAttribute('id'));
                                                                                             $CONF_DONATION_TAX_RECEIPT_PARAMETERS[$Y_ID][Page][$PageID][$iPart][$FieldID][$ItemID] = array();

                                                                                             foreach($ItemNode->childNodes as $ParamItemNode)
                                                                                             {
                                                                                                 if ($ParamItemNode->nodeType == XML_ELEMENT_NODE)
                                                                                                 {
                                                                                                     // Parameters are Text, PosX, PosY... constants
                                                                                                     $iParamField = getXmlNodeValue($ParamItemNode->nodeName);
                                                                                                     if (defined($iParamField))
                                                                                                     {
                                                                                                         $iParamField = eval("return ".$iParamField.";");
                                                                                                     }

                                                                                                     if (getXmlNodeValue($ParamItemNode->nodeName) == 'Items')
                                                                                                     {
                                                                                                         // Several values for the parameter
                                                                                                         $CONF_DONATION_TAX_RECEIPT_PARAMETERS[$Y_ID][Page][$PageID][$iPart][$FieldID][$ItemID][$iParamField] = array();

                                                                                                         // Get values
                                                                                                         $ItemValues = $ParamItemNode->getElementsByTagName('value');
                                                                                                         foreach($ItemValues as $ItemValue)
                                                                                                         {
                                                                                                             $CONF_DONATION_TAX_RECEIPT_PARAMETERS[$Y_ID][Page][$PageID][$iPart][$FieldID][$ItemID][$iParamField][] = getXmlNodeValue($ItemValue->nodeValue);
                                                                                                         }
                                                                                                     }
                                                                                                     else
                                                                                                     {
                                                                                                         // Single parameter
                                                                                                         $CONF_DONATION_TAX_RECEIPT_PARAMETERS[$Y_ID][Page][$PageID][$iPart][$FieldID][$ItemID][$iParamField] = getXmlNodeValue($ParamItemNode->nodeValue);
                                                                                                     }
                                                                                                 }
                                                                                             }
                                                                                         }
                                                                                     }
                                                                                 }
                                                                                 else
                                                                                 {
                                                                                     // Parameters are Text, PosX, PosY... constants
                                                                                     $iParamField = getXmlNodeValue($ParamFieldNode->nodeName);
                                                                                     if (defined($iParamField))
                                                                                     {
                                                                                         $iParamField = eval("return ".$iParamField.";");
                                                                                     }

                                                                                     $CONF_DONATION_TAX_RECEIPT_PARAMETERS[$Y_ID][Page][$PageID][$iPart][$FieldID][$iParamField] = getXmlNodeValue($ParamFieldNode->nodeValue);
                                                                                 }
                                                                             }
                                                                         }
                                                                     }
                                                                 }
                                                             }
                                                         }
                                                     }
                                                 }
                                             }
                                         }
                                     }
                                 }
                             }

                             unset($XmlParser, $Years);
                             break;
                     }
                     break;

                 default:
                     // For a default treatment
                     $ParamName = strToUpper($ParamName);

                     // Reinit
                     $GLOBALS[$ParamName] = null;

                     // Load donation tax receipt parameters for each school year
                     switch($RecordParamValue['ConfigParameterType'])
                     {
                         case CONF_PARAM_TYPE_XML:
                             // Add header for the charset
                             $RecordParamValue['ConfigParameterValue'] = "<?xml version=\"1.0\" encoding=\"".$GLOBALS['CONF_CHARSET']."\" ?>"
                                                                         .$RecordParamValue['ConfigParameterValue'];

                             // Load XML
                             $XmlParser = new DomDocument();
                             $XmlParser->loadXML($RecordParamValue['ConfigParameterValue']);
                             $GLOBALS[$ParamName] = getXmlToArray($XmlParser->firstChild);
                             break;

                         case CONF_PARAM_TYPE_STRING:
                             $GLOBALS[$ParamName] = trim($RecordParamValue['ConfigParameterValue']);
                             break;

                         case CONF_PARAM_TYPE_BOOLEAN:
                             $sTmpValue = strToUpper(trim($RecordParamValue['ConfigParameterValue']));
                             if (in_array($sTmpValue, array('1', 'TRUE')))
                             {
                                 $GLOBALS[$ParamName] = TRUE;
                             }
                             else
                             {
                                 $GLOBALS[$ParamName] = FALSE;
                             }
                             break;

                         case CONF_PARAM_TYPE_NUMBER:
                             // Integer or float
                             $sTmpValue = trim($RecordParamValue['ConfigParameterValue']);
                             if (strpos($sTmpValue, '.') !== FALSE)
                             {
                                 // Float
                                 $GLOBALS[$ParamName] = (float)$RecordParamValue['ConfigParameterValue'];
                             }
                             else
                             {
                                 // Integer
                                 $GLOBALS[$ParamName] = (integer)$RecordParamValue['ConfigParameterValue'];
                             }
                             break;

                         case CONF_PARAM_TYPE_CONST:
                             // PHP constant declared in the application
                             if (defined($RecordParamValue['ConfigParameterValue']))
                             {
                                 $GLOBALS[$ParamName] = eval("return ".$RecordParamValue['ConfigParameterValue'].";");
                             }
                             break;
                     }
                     break;
             }
         }
     }

     unset($ArrayDbConfParams);
 }


/**
 * Load config parameters from database in config variables coming from Config.php
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-17
 *
 * @param $DbConnection             DB object              Object of the opened database connection
 * @param $ArrayParams              Mixed array            Contains names of config translation messages to load.
 *                                                         If empty, load all languages variables
 */
 function loadDbConfigLanguages($DbConnection, $ArrayParams = array())
 {
     // These language variables come from *.lang.php
     if (empty($ArrayParams))
     {
         // We load all language variables
         $sPrefix = 'LANG_';
         $iNbChars = strlen($sPrefix);
         foreach($GLOBALS as $VarName => $VarValue)
         {
             if (substr($VarName, 0, $iNbChars) == $sPrefix)
             {
                 $ArrayParams[] = $VarName;
             }
         }
     }

     // Get all languages variables from DB
     $ArrayDbLanguages = array();
     $ArrayConfigLanguages = dbSearchConfigLanguages($DbConnection, array('ConfigLanguageLang' => $GLOBALS['CONF_LANG']),
                                                     "ConfigLanguageID", 1, 0);

     if ((isset($ArrayConfigLanguages['ConfigLanguageID'])) && (!empty($ArrayConfigLanguages['ConfigLanguageID'])))
     {
         foreach($ArrayConfigLanguages['ConfigLanguageName'] as $c => $CurrentLangName)
         {
             $ArrayDbLanguages[$CurrentLangName] = $ArrayConfigLanguages['ConfigLanguageValue'][$c];
         }
     }

     unset($ArrayConfigLanguages);

     // Set new translation to languages variables
     foreach($ArrayParams as $p => $LangVarName)
     {
         $LangVarName = strToUpper($LangVarName);

         // Declare this global variable in this function
         if (isset($GLOBALS[$LangVarName]))
         {
             global ${$LangVarName};
         }

         // We check if there is a translation in DB for this language variable
         if (isset($ArrayDbLanguages[$LangVarName]))
         {
             // Yes : reinit
             $GLOBALS[$LangVarName] = null;

             // Set the value
             $GLOBALS[$LangVarName] = trim($ArrayDbLanguages[$LangVarName]);
         }
     }

     unset($ArrayDbLanguages);
 }
?>