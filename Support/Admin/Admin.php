<?php
/* Copyright (C) 2012 Calandreta Del Pa�s Murethin
 *
 * This file is part of CanteenCalandreta.
 *
 * CanteenCalandreta is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CanteenCalandreta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CanteenCalandreta; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * Support module : index of the "Admin" module
 *
 * @author Christophe javouhey
 * @version 3.9
 *     - 2021-11-15 : v3.7. Remove $Msgs
 *     - 2022-10-13 : v3.8. Check last versions on GIT of project
 *     - 2022-10-17 : v3.9. All configuration and translation variables loaded from database
 *                    in the /GUI/GraphicInterface.php file and allow to download, simulate
 *                    and install a new version. Remove initStartTime() function
 *
 * @since 2016-10-21
 */

 // Include the graphic primitives library
 require '../../GUI/GraphicInterface.php';

 // Create "supporter" session or use the opened "supporter" session
 session_start();

 // Redirect the user to the login page index.php if he isn't loggued
 setRedirectionToLoginPage();

 // Connection to the database
 $DbCon = dbConnection();

 //################################ FORM PROCESSING ##########################
 $sBranch = '';
 $sActionToDo = '';

 if (isSet($_SESSION["SupportMemberID"]))
 {
     if (!empty($_POST["bDownloadVersion"]))
     {
         $sActionToDo = 'download';
     }
     elseif (!empty($_POST["bSimulate"]))
     {
         $sActionToDo = 'simulate';
     }
     elseif (!empty($_POST["bInstallVersion"]))
     {
         $sActionToDo = 'install';
     }

     $sBranch = trim(strip_tags($_POST["radVersion"]));
     if (empty($sBranch))
     {
         $sActionToDo = '';
     }
 }
 //################################ END FORM PROCESSING ##########################

 initGraphicInterface(
                      $LANG_INTRANET_NAME,
                      array(
                            '../../GUI/Styles/styles.css' => 'screen',
                            '../Styles_Support.css' => 'screen'
                           ),
                      array(),
                      ''
                     );
 openWebPage();

 // Display the header of the application
 displayHeader($LANG_INTRANET_HEADER);

 // Display the main menu at the top of the web page
 displaySupportMainMenu(1);

 // Content of the web page
 openArea('id="content"');

 // Display the "Stats" and the "parameters" contextual menus if the supporter isn't logged, an empty contextual menu otherwise
 if (isSet($_SESSION["SupportMemberID"]))
 {
     // Open the contextual menu area
     openArea('id="contextualmenu"');

     displaySupportMemberContextualMenu("admin", 1, 0);
     displaySupportMemberContextualMenu("parameters", 1, 0);

     // Display information about the logged user
     displayLoggedUser($_SESSION);

     // Close the <div> "contextualmenu"
     closeArea();

     openArea('id="page"');
 }

 // Display the informations, forms, etc. on the right of the web page
 displayTitlePage($LANG_ADMIN_INDEX_PAGE_TITLE, 2);

 openParagraph();
 echo $LANG_ADMIN_INDEX_PAGE_PARAGRAPH_ONE;
 closeParagraph();

 displaySeparator($LANG_ADMIN_INDEX_PAGE_AVAILABLE_STATS);

 openParagraph();
 openForm("FormDetailsNewVersion", "post", "Admin.php", "", "");

 displayStyledText($LANG_ADMIN_INDEX_PAGE_CHECK_VERSIONS_LIST, 'AdminVersionsList');

 // Get last committed dates of branches of the project
 $ArrayVersions = checkNewVersions($CONF_MODE_CHECK_VERSIONS);
 if (!empty($ArrayVersions))
 {
     echo "<ul class=\"AdminVersionsList\">\n";

     foreach($ArrayVersions as $Version => $CurrentVerInfos)
     {
         $bSelected = FALSE;
         if ($Version == $sBranch)
         {
             $bSelected = TRUE;
         }

         echo "<li>".generateInputField('radVersion', 'radio', 1, 1, $Version, $Version, FALSE, $bSelected, '', '').' '
                    .generateStyledLinkText("$Version : ".date($CONF_DATE_DISPLAY_FORMAT.' '.$CONF_TIME_DISPLAY_FORMAT, strtotime($CurrentVerInfos['committed_date'])),
                                            $CONF_MODE_CHECK_VERSIONS_PROJECT_URL."-/tree/$Version", '', '', '_blank');

         if (!empty($CurrentVerInfos['version']))
         {
             // Display the version name
             echo " (".$CurrentVerInfos['version'].")";
         }

         echo "</li>\n";
     }

     echo "</ul>\n";
 }

 displayBR(1);

 // We check if there is a new main version available
 $bNewVersionAvailable = checkUpdateVersionNeeded($DbCon);
 if ($bNewVersionAvailable)
 {
     displayStyledText($LANG_NEW_VERSION_AVAILABLE_TO_INSTALL, 'AdminNewVersionToInstall');
 }
 else
 {
     displayStyledText($LANG_NEW_VERSION_UP_TO_DATE, 'AdminVersionUpToDate');
 }

 displayBR(3);

 insertInputField("bDownloadVersion", "submit", "", "", $GLOBALS["LANG_NEW_VERSION_DOWNLOAD_VERSION_BUTTON_TIP"], $GLOBALS["LANG_NEW_VERSION_DOWNLOAD_VERSION_BUTTON_CAPTION"]);
 echo "&nbsp;&nbsp;&nbsp;&nbsp;";
 insertInputField("bSimulate", "submit", "", "", $GLOBALS["LANG_NEW_VERSION_SIMULATE_VERSION_BUTTON_TIP"], $GLOBALS["LANG_NEW_VERSION_SIMULATE_VERSION_BUTTON_CAPTION"]);
 echo "&nbsp;&nbsp;&nbsp;&nbsp;";
 insertInputField("bInstallVersion", "submit", "", "", $GLOBALS["LANG_NEW_VERSION_INSTALL_VERSION_BUTTON_TIP"], $GLOBALS["LANG_NEW_VERSION_INSTALL_VERSION_BUTTON_CAPTION"]);
 closeForm();

 closeParagraph();

 switch($sActionToDo)
 {
     case 'download':
         // Download the version of the selected branch
         openParagraph();

         $bResult = downloadNewVersion($sBranch);
         if ($bResult)
         {
             displayStyledText($LANG_NEW_VERSION_DOWNLOAD_SUCCESS, 'AdminResultSuccess');
         }
         else
         {
             displayStyledText($LANG_NEW_VERSION_DOWNLOAD_ERROR, 'AdminResultError');
         }

         closeParagraph();
         break;

     case 'simulate':
         // Simulate the installation of the version of the selected branch (if downloaded before !)
         openParagraph();

         $bResult = installNewVersion($DbCon, $sBranch, TRUE);

         closeParagraph();
         break;

     case 'install':
         // Install the version of the selected branch (if downloaded before !)
         openParagraph();
         $bResult = installNewVersion($DbCon, $sBranch, FALSE);
         if ($bResult)
         {
             // The new version is installed !
             displayStyledText($LANG_NEW_VERSION_INSTALL_VERSION_SUCCESS, 'AdminResultSuccess');

             logEvent($DbCon, EVT_SYSTEM, EVT_SERV_INSTALL_VERSION, EVT_ACT_UPDATE, $_SESSION['SupportMemberID'], 0,
                      array(
                            'Branch' => $sBranch,
                            'Version' => $ArrayVersions[$sBranch]['version'],
                            'CommitID' => $ArrayVersions[$sBranch]['id'],
                            'CommitDate' => date('Y-m-d H:i:s', strtotime($CurrentVerInfos['committed_date']))
                           ));
         }
         else
         {
             // Error during the installation
             displayStyledText($LANG_NEW_VERSION_INSTALL_VERSION_ERROR, 'AdminResultError');
         }

         closeParagraph();
         break;
 }

 // Release the connection to the database
 dbDisconnection($DbCon);

 // To measure the execution script time
 if ($CONF_DISPLAY_EXECUTION_TIME_SCRIPT)
 {
     openParagraph('InfoMsg');
     initEndTime();
     displayExecutionScriptTime('ExecutionTime');
     closeParagraph();
 }

 if (isSet($_SESSION["SupportMemberID"]))
 {
     // Close the <div> "Page"
     closeArea();
 }

 // Close the <div> "content"
 closeArea();

 // Footer of the application
 displayFooter($LANG_INTRANET_FOOTER);

 // Close the web page
 closeWebPage();

 closeGraphicInterface();
?>