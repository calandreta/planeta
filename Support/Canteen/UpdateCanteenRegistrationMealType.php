<?php
/* Copyright (C) 2012 Calandreta Del Pa�s Murethin
 *
 * This file is part of CanteenCalandreta.
 *
 * CanteenCalandreta is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CanteenCalandreta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CanteenCalandreta; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * Support module : allow a supporter to update the meal type of a canteen registration of a child.
 * The supporter must be logged to update the meal type of the canteen registration.
 *
 * @author Christophe Javouhey
 * @version 3.9
 * @since 2022-10-17
 */

 // Include the graphic primitives library
 require '../../GUI/GraphicInterface.php';

 // Create "supporter" session or use the opened "supporter" session
 session_start();

 // Redirect the user to the login page index.php if he isn't loggued
 setRedirectionToLoginPage();

 // Connection to the database
 $DbCon = dbConnection();

 $UrlRedirection = '';

 if (!empty($_POST["lChildID"]))
 {
     $ChildID = (integer)strip_tags($_POST["lChildID"]);
 }
 else
 {
     if (!empty($_GET["lChildID"]))
     {
         $ChildID = (integer)strip_tags($_GET["lChildID"]);
     }
     else
     {
         $ChildID = 0;  // No child selected
     }
 }

 //################################ FORM PROCESSING ##########################
 if (!empty($_POST["bSubmit"]))
 {
     if (isSet($_SESSION["SupportMemberID"]))
     {
         $ContinueProcess = TRUE; // Used to check that the parameters are correct

         $ChildID = strip_tags($_POST["hidChildID"]);
         if ($ChildID == 0)
         {
             // Error
             $ContinueProcess = FALSE;
         }

         // Get canteen registrations to update
         $ArrayCanteenRegistrationsToUpdate = array();
         foreach($_POST as $FiledName => $CurrentValue)
         {
             if (substr($FiledName, 0, 10) == 'lMealType_')
             {
                 $ArrayTmp = explode('_', $FiledName);
                 $CanteenRegistrationID = $ArrayTmp[1];
                 $CanteenRegistrationRecord = getTableRecordInfos($DbCon, "CanteenRegistrations", $CanteenRegistrationID);

                 // We check the canteen registration is for the right child and if we must update this canteen registration
                 // because the meal type has changed
                 if ((!empty($CanteenRegistrationRecord)) && (isset($CanteenRegistrationRecord['ChildID']))
                     && ($CanteenRegistrationRecord['ChildID'] == $ChildID)
                     && ($CurrentValue != $CanteenRegistrationRecord['CanteenRegistrationWithoutPork']))
                 {
                     $ArrayCanteenRegistrationsToUpdate[$CanteenRegistrationID] = array(
                                                                                        'NewMealType' => $CurrentValue,
                                                                                        'OldRecord' => $CanteenRegistrationRecord
                                                                                       );
                 }
             }
         }

         $UrlRedirection = $CONF_ROOT_DIRECTORY."Support/Canteen/UpdateCanteenRegistrationMealType.php?lChildID=$ChildID"; // For the redirection

         if ($ContinueProcess)
         {
             $ConfirmationCaption = $LANG_CONFIRMATION;
             $ConfirmationSentence = $LANG_CONFIRM_CANTEEN_MEAL_TYPE_UPDATED;
             $ConfirmationStyle = "ConfirmationMsg";

             foreach($ArrayCanteenRegistrationsToUpdate as $CanteenRegistrationID => $CurrentRecord)
             {
                 // The meal type has changed : we must update this canteen registration
                 $UpdatedID = dbUpdateCanteenRegistration($DbCon, $CanteenRegistrationID, NULL, $CurrentRecord['OldRecord']['CanteenRegistrationForDate'],
                                                          $ChildID, NULL, NULL, $CurrentRecord['NewMealType']);

                 if ($UpdatedID > 0)
                 {
                     // Log event
                     logEvent($DbCon, EVT_CANTEEN, EVT_SERV_PLANNING, EVT_ACT_UPDATE, $_SESSION['SupportMemberID'], $CanteenRegistrationID);
                 }
                 else
                 {
                     // Error
                     $ConfirmationCaption = $LANG_ERROR;
                     $ConfirmationSentence = $LANG_ERROR_UPDATE_MEAL_TYPE;
                     $ConfirmationStyle = "ErrorMsg";
                 }
             }
         }
         else
         {
             // Error
             $ConfirmationCaption = $LANG_ERROR;
             $ConfirmationSentence = $LANG_ERROR_UPDATE_MEAL_TYPE;
             $ConfirmationStyle = "ErrorMsg";
         }
     }
     else
     {
         // ERROR : the supporter isn't logged
         $ConfirmationCaption = $LANG_ERROR;
         $ConfirmationSentence = $LANG_ERROR_NOT_LOGGED;
         $ConfirmationStyle = "ErrorMsg";

         $UrlRedirection = $CONF_ROOT_DIRECTORY."Support/Canteen/UpdateCanteenRegistrationMealType.php";  // For the redirection
     }
 }

 //################################ END FORM PROCESSING ##########################

 if (empty($UrlRedirection))
 {
     initGraphicInterface(
                          $LANG_INTRANET_NAME,
                          array(
                                '../../GUI/Styles/styles.css' => 'screen',
                                '../Styles_Support.css' => 'screen'
                               ),
                          array(
                                '../Verifications.js'
                               ),
                          'WhitePage'
                         );
 }
 else
 {
     // To display confirmation or error message
     initGraphicInterface(
                          $LANG_INTRANET_NAME,
                          array(
                                '../../GUI/Styles/styles.css' => 'screen',
                                '../Styles_Support.css' => 'screen'
                               ),
                          array($CONF_ROOT_DIRECTORY."Common/JSRedirection/Redirection.js"),
                          'WhitePage',
                          "Redirection('$UrlRedirection', $CONF_TIME_LAG)"
                     );
 }

 // Display invisible link to go directly to content
 displayStyledLinkText($LANG_GO_TO_CONTENT, '#ChildCanteenRegistrations', 'Accessibility');

 // Content of the web page
 openArea('id="content"');

 if (empty($UrlRedirection))
 {
     displayTitlePage($LANG_SUPPORT_UPDATE_CANTEEN_REGISTRATION_MEAL_TYPE_TITLE, 2);

     openParagraph();
     displayStyledText($LANG_SUPPORT_UPDATE_CANTEEN_REGISTRATION_MEAL_TYPE_INTRODUCTION, "");
     closeParagraph();

     // We check if the supporter is allowed to upade the meal typ of the canteen registrations
     if (in_array($_SESSION['SupportMemberStateID'], $CONF_CANTEEN_UPDATE_ALLOW_CHANGE_MEAL_TYPE))
     {
          displayChangeCanteenRegistrationMealTypeForm($DbCon, "UpdateCanteenRegistrationMealType.php", $ChildID,
                                                       $CONF_ACCESS_APPL_PAGES[FCT_CANTEEN_PLANNING], $CONF_CANTEEN_VIEWS_RESTRICTIONS);
     }
     else
     {
         openFrame($LANG_ERROR);
         displayStyledText($LANG_ERROR_NOT_CHANGE_CANTEEN_REGISTRATION_MEAL_TYPE, 'ErrorMsg');
         closeFrame();
     }
 }
 else
 {
     // Display confirmation or error message
     openFrame($ConfirmationCaption);
     displayStyledText($ConfirmationSentence, $ConfirmationStyle);
     closeFrame();
 }

 // Release the connection to the database
 dbDisconnection($DbCon);

 // To measure the execution script time
 if ($CONF_DISPLAY_EXECUTION_TIME_SCRIPT)
 {
     openParagraph('InfoMsg');
     initEndTime();
     displayExecutionScriptTime('ExecutionTime');
     closeParagraph();
 }

 // Close the <div> "content"
 closeArea();

 closeGraphicInterface();
?>