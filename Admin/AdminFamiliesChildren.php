<?php
/* Copyright (C) 2012 Calandreta Del Pa�s Murethin
 *
 * This file is part of CanteenCalandreta.
 *
 * CanteenCalandreta is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CanteenCalandreta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CanteenCalandreta; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * Admin module : update Families, Children, Towns and HistoLevelsChildren tables.
 * If no entry in SupportMembers table for the family, we create an account.
 *
 * @author Christophe Javouhey
 * @version 3.9
 *     - 2013-06-21 : Taken into account the new structure of the CONF_CLASSROOMS variable
 *                    (includes school year)
 *     - 2016-11-02 : v3.0. Load some configuration variables from database
 *     - 2022-10-04 : v3.8. Allow to import all fields of families
 *     - 2022-12-16 : v3.9. Link FamilyID to a support member
 *
 * @since 2012-04-06
 */


 if (!function_exists('getIntranetRootDirectoryHDD'))
 {
    /**
     * Give the path of the Intranet root directory on the HDD
     *
     * @author Christophe Javouhey
     * @version 1.1
     *     - 2021-12-27 : v1.1. Replace $sLocalDir{0} by substr($sLocalDir, 0, 1) for PHP8
     *
     * @since 2012-03-20
     *
     * @return String             Intranet root directory on the HDD
     */
     function getIntranetRootDirectoryHDD()
     {
         $sLocalDir = str_replace(array("\\"), array("/"), dirname(__FILE__)).'/';
         $bUnixOS = FALSE;
         if (substr($sLocalDir, 0, 1) == '/')
         {
             $bUnixOS = TRUE;
         }

         $ArrayTmp = explode('/', $sLocalDir);

         $iPos = array_search("CanteenCalandreta", $ArrayTmp);
         if ($iPos !== FALSE)
         {
             $sLocalDir = '';
             if ($bUnixOS)
             {
                 $sLocalDir = '/';
             }

             for($i = 0; $i <= $iPos; $i++)
             {
                 $sLocalDir .= $ArrayTmp[$i].'/';
             }
         }

         return $sLocalDir;
     }
 }


 function formatName($Name)
 {
     // Format a lastname or a firstname
     if (!empty($Name))
     {
         $Name = trim($Name);

         // Check the separator
         $ArraySeparators = array("/", "-", " ", "'");
         $Separator = NULL;
         foreach($ArraySeparators as $s => $Sep)
         {
             if (stripos($Name, $Sep))
             {
                 $Separator = $Sep;
                 break;
             }
         }

         if (is_null($Separator))
         {
             $Name = ucfirst(strtolower($Name));
         }
         else
         {
             $ArrayTmp = explode($Separator, $Name);
             foreach($ArrayTmp as $a => $sTmp)
             {
                 $ArraySecSepName = explode("'", $sTmp);
                 foreach($ArraySecSepName as $s => $CurrName)
                 {
                     $ArraySecSepName[$s] = ucfirst(strtolower($CurrName));
                 }

                 $ArrayTmp[$a] = implode("'", $ArraySecSepName);
             }

             $Name = implode($Separator, $ArrayTmp);
         }
     }

     return $Name;
 }


 // Configuration variables
 $DOCUMENT_ROOT = getIntranetRootDirectoryHDD();
 $Filename = basename(str_replace(array("Admin"), array(), __FILE__), ".php");
 $CONF_ADMIN_INPUT_FILE_PATH = dirname(__FILE__)."/Import".$Filename.".csv";

 include_once($DOCUMENT_ROOT.'GUI/GraphicInterface.php');

 define('ADMIN_DEFAULT_FAMILY_MONTHLY_CONTRIBUTION_MODE', MC_DEFAULT_MODE);
 define('ADMIN_DEFAULT_FAMILY_NB_MEMBERS', 1);
 define('ADMIN_DEFAULT_FAMILY_NB_POWERED_MEMBERS', 0);
 define('ADMIN_DEFAULT_FAMILY_MONTHLY_NURSERY_MODE', 0);
 define('ADMIN_DEFAULT_FAMILY_ANNUAL_CONTRIBUTION_BALANCE', 0.00);
 define('ADMIN_DEFAULT_FAMILY_BALANCE', 0.00);
 define('ADMIN_DEFAULT_CHILD_WITHOUT_PORK', 0);
 define('ADMIN_DEFAULT_CHILD_GRADE', 0);
 define('ADMIN_DEFAULT_CHILD_CLASSROOM', 0);
 define('ADMIN_DEFAULT_SUPPORT_MEMBER_STATE_ID', 5);
 define('ADMIN_DEFAULT_SUPPORT_MEMBER_PWD', "calandreta");

 $CONF_ADMIN_REQUIRED_FIELDS = array(
                                     "Towns" => array("TownName", "TownCode"),
                                     "Families" => array("FamilyLastname", "FamilyDate", "FamilyNbMembers", "FamilyMainEmail"),
                                     "Children" => array("ChildFirstname", "ChildSchoolDate", "ChildGrade", "ChildClass",
                                                         "ChildWithoutPork")
                                    );

 $DbCon = dbConnection();

 // Load all configuration variables from database
 loadDbConfigParameters($DbCon, array());

 // Read the CSV schema file
 $SchemaCSVFile = file(dirname(__FILE__)."/Schema".$Filename.".csv");

 // Read the CSV data file
 $DataCSVFile = getContentCSVFile($CONF_ADMIN_INPUT_FILE_PATH, 200000);

 // Check if the first line of the CSV file is the same as the schema
 $Schema = trim($SchemaCSVFile[0]);

 $ArrayColumns = explode(';', $Schema);
 foreach($ArrayColumns as $c => $ColName)
 {
     $ArrayColumns[$c] = strtolower($ColName);
 }

 if (count($DataCSVFile[0]) == count($ArrayColumns))
 {
     $CurrentSchoolYear = getSchoolYear(date('Y-m-d'));

     foreach($DataCSVFile as $i => $CurrentLine)
     {
         // We don't treat the first line (headers)
         if ($i > 0)
         {
             // Init the structure of data
             $iArrayColumnsSize = count($ArrayColumns);
             $iCurrentLineSize = count($CurrentLine);
             if ($iCurrentLineSize < $iArrayColumnsSize)
             {
                 $CurrentLine = array_pad($CurrentLine, $iArrayColumnsSize, '');
             }

             $RecordToImport = array_combine($ArrayColumns, $CurrentLine);
             foreach($RecordToImport as $Field => $Value)
             {
                 $Value = strip_tags(trim($Value));
                 switch(strtolower($Field))
                 {
                     /**** Data for the Families table ****/
                     case "nom":
                         $RecordToImport['Families']['FamilyLastname'] = formatName($Value);
                         break;

                     case "date premi�re inscription":
                     case "date arriv�e � l'�cole":
                         if (empty($Value))
                         {
                             // No date, by default, the first day of the current school year
                             $Value = $CONF_SCHOOL_YEAR_START_DATES[$CurrentSchoolYear];
                         }
                         elseif (strpos($Value, '/') !== FALSE)
                         {
                             // Frensh format
                             $Value = formatedDate2EngDate($Value);
                         }

                         $RecordToImport['Families']['FamilyDate'] = $Value;
                         $RecordToImport['Children']['ChildSchoolDate'] = $Value;
                         break;

                     case "adresse e-mail principale":
                         if (empty($Value))
                         {
                             $Value = str_replace(array(" ", "'"), array("", "-"), $RecordToImport['Families']['FamilyLastname']);
                             $Value .= "@test-calandreta.fr";
                         }

                         $RecordToImport['Families']['FamilyMainEmail'] = strtolower($Value);
                         break;

                     case "adresse e-mail secondaire":
                         $RecordToImport['Families']['FamilySecondEmail'] = strtolower($Value);
                         break;

                     case "membre du ca adresse e-mail principale":
                         $RecordToImport['Families']['FamilyMainEmailInCommittee'] = 0;
                         $Value = strtolower($Value);
                         if (($Value == "oui") || ($Value == "1"))
                         {
                             $RecordToImport['Families']['FamilyMainEmailInCommittee'] = 1;
                         }
                         break;

                     case "membre du ca adresse e-mail secondaire":
                         $RecordToImport['Families']['FamilySecondEmailInCommittee'] = 0;
                         $Value = strtolower($Value);
                         if (($Value == "oui") || ($Value == "1"))
                         {
                             $RecordToImport['Families']['FamilySecondEmailInCommittee'] = 1;
                         }
                         break;

                     case "famille de regent":
                         $RecordToImport['Families']['FamilySpecialAnnualContribution'] = 0;
                         $Value = strtolower($Value);
                         if (($Value == "oui") || ($Value == "1"))
                         {
                             $RecordToImport['Families']['FamilySpecialAnnualContribution'] = 1;
                         }
                         break;

                     case "nb votes":
                         if (empty($Value))
                         {
                             $RecordToImport['Families']['FamilyNbMembers'] = ADMIN_DEFAULT_FAMILY_NB_MEMBERS;
                         }
                         else
                         {
                             $RecordToImport['Families']['FamilyNbMembers'] = $Value;
                         }
                         break;

                     case "nb votes avec pouvoir":
                         if (empty($Value))
                         {
                             $RecordToImport['Families']['FamilyNbPoweredMembers'] = ADMIN_DEFAULT_FAMILY_NB_POWERED_MEMBERS;
                         }
                         else
                         {
                             $RecordToImport['Families']['FamilyNbPoweredMembers'] = $Value;
                         }
                         break;

                     case "type de membre":
                         $Value = strtolower($Value);
                         if ($Value !== '')
                         {
                             switch($Value)
                             {
                                 case "2":
                                 case "t1":
                                     $RecordToImport['Families']['FamilyMonthlyContributionMode'] = MC_FAMILY_COEFF_1_MODE;
                                     break;

                                 case "3":
                                 case "t2":
                                     $RecordToImport['Families']['FamilyMonthlyContributionMode'] = MC_FAMILY_COEFF_2_MODE;
                                     break;

                                 case "4":
                                 case "t3":
                                     $RecordToImport['Families']['FamilyMonthlyContributionMode'] = MC_FAMILY_COEFF_3_MODE;
                                     break;

                                 case "5":
                                 case "t4":
                                     $RecordToImport['Families']['FamilyMonthlyContributionMode'] = MC_FAMILY_COEFF_4_MODE;
                                     break;

                                 case "6":
                                 case "t5":
                                     $RecordToImport['Families']['FamilyMonthlyContributionMode'] = MC_FAMILY_COEFF_5_MODE;
                                     break;

                                 case "7":
                                 case "t6":
                                     $RecordToImport['Families']['FamilyMonthlyContributionMode'] = MC_FAMILY_COEFF_6_MODE;
                                     break;

                                 default:
                                     $RecordToImport['Families']['FamilyMonthlyContributionMode'] = MC_DEFAULT_MODE;
                                     break;
                             }
                         }
                         else
                         {
                             $RecordToImport['Families']['FamilyMonthlyContributionMode'] = ADMIN_DEFAULT_FAMILY_MONTHLY_CONTRIBUTION_MODE;
                         }
                         break;

                     case "mode paiement garderie":
                         $Value = strtolower($Value);
                         if ($Value !== '')
                         {
                             if (in_array($Value, array('0', '1')))
                             {
                                 $RecordToImport['Families']['FamilyMonthlyNurseryMode'] = $Value;
                             }
                             else
                             {
                                 switch($Value)
                                 {
                                     case "forfait":
                                         $RecordToImport['Families']['FamilyMonthlyNurseryMode'] = 1;
                                         break;

                                     case "occasionnel":
                                     default:
                                         $RecordToImport['Families']['FamilyMonthlyNurseryMode'] = 0;
                                         break;
                                 }
                             }
                         }
                         else
                         {
                             $RecordToImport['Families']['FamilyMonthlyNurseryMode'] = ADMIN_DEFAULT_FAMILY_MONTHLY_NURSERY_MODE;
                         }
                         break;

                     case "solde cotisation annuelle":
                         if ($Value != '')
                         {
                             if ($Value == '*')
                             {
                                 $RecordToImport['Families']['FamilyAnnualContributionBalance'] = ADMIN_DEFAULT_FAMILY_ANNUAL_CONTRIBUTION_BALANCE;
                             }
                             else
                             {
                                 $RecordToImport['Families']['FamilyAnnualContributionBalance'] = str_replace(array(","), array("."), $Value);
                             }
                         }
                         break;

                     case "solde":
                     case "solde factures mensuelles":
                         if (empty($Value))
                         {
                             $RecordToImport['Families']['FamilyBalance'] = ADMIN_DEFAULT_FAMILY_BALANCE;
                         }
                         else
                         {
                             $RecordToImport['Families']['FamilyBalance'] = str_replace(array(","), array("."), $Value);
                         }
                         break;

                     /**** Data for the Children table ****/
                     case "pr�nom":
                         $RecordToImport['Children']['ChildFirstname'] = formatName($Value);
                         break;

                     case "niveau":
                         $RecordToImport['Children']['ChildGrade'] = NULL;
                         $iPos = array_search(strtoupper($Value), $CONF_GRADES);
                         if ($iPos !== FALSE)
                         {
                             $RecordToImport['Children']['ChildGrade'] = $iPos;
                         }
                         else
                         {
                             $RecordToImport['Children']['ChildGrade'] = ADMIN_DEFAULT_CHILD_GRADE;
                         }
                         break;

                     case "classe":
                         $RecordToImport['Children']['ChildClass'] = NULL;
                         $iPos = array_search(strtoupper($Value), $CONF_CLASSROOMS[$CurrentSchoolYear]);
                         if ($iPos !== FALSE)
                         {
                             $RecordToImport['Children']['ChildClass'] = $iPos;
                         }
                         else
                         {
                             $RecordToImport['Children']['ChildClass'] = ADMIN_DEFAULT_CHILD_CLASSROOM;
                         }
                         break;

                     case "repas sans porc":
                     case "type de repas":
                         $Value = strtolower($Value);
                         switch($Value)
                         {
                             case "non":
                             case "1":
                                 $RecordToImport['Children']['ChildWithoutPork'] = 0;
                                 break;

                             case "oui":
                             case "2":
                                 $RecordToImport['Children']['ChildWithoutPork'] = 1;
                                 break;

                             case "panier-repas":
                             case "3":
                                 $RecordToImport['Children']['ChildWithoutPork'] = 2;
                                 break;

                             default:
                                 $RecordToImport['Children']['ChildWithoutPork'] = ADMIN_DEFAULT_CHILD_WITHOUT_PORK;
                                 break;
                         }
                         break;

                     /**** Data for the Towns table ****/
                     case "code postal":
                         $RecordToImport['Towns']['TownCode'] = NULL;
                         if (strlen($Value) == 5)
                         {
                             $RecordToImport['Towns']['TownCode'] = $Value;
                         }
                         break;

                     case "commune":
                         $RecordToImport['Towns']['TownName'] = ucfirst(strtolower($Value));
                         break;
                 }

                 unset($RecordToImport[$Field]);
             }

             // We set mandatory fields which have no value
             if (!isset($RecordToImport['Families']['FamilyNbMembers']))
             {
                 $RecordToImport['Families']['FamilyNbMembers'] = ADMIN_DEFAULT_FAMILY_NB_MEMBERS;
             }

             if (!isset($RecordToImport['Families']['FamilyNbPoweredMembers']))
             {
                 $RecordToImport['Families']['FamilyNbPoweredMembers'] = ADMIN_DEFAULT_FAMILY_NB_POWERED_MEMBERS;
             }

             if (!isset($RecordToImport['Families']['FamilySpecialAnnualContribution']))
             {
                 $RecordToImport['Families']['FamilySpecialAnnualContribution'] = 0;
             }

             if (!isset($RecordToImport['Families']['FamilyMonthlyContributionMode']))
             {
                 $RecordToImport['Families']['FamilyMonthlyContributionMode'] = ADMIN_DEFAULT_FAMILY_MONTHLY_CONTRIBUTION_MODE;
             }

             if (!isset($RecordToImport['Families']['FamilyMonthlyNurseryMode']))
             {
                 $RecordToImport['Families']['FamilyMonthlyNurseryMode'] = ADMIN_DEFAULT_FAMILY_MONTHLY_NURSERY_MODE;
             }

             if (!isset($RecordToImport['Families']['FamilyAnnualContributionBalance']))
             {
                 // How many contributions?
                 $NbContributions = $RecordToImport['Families']['FamilyNbMembers'] + $RecordToImport['Families']['FamilyNbPoweredMembers'];

                 // Price off for annual contribution?
                 $AnnualPriceOff = 0;
                 if ($RecordToImport['Families']['FamilySpecialAnnualContribution'] == 1)
                 {
                     // Special family : we remove 1 powered number to compute the annual contribution
                     if ($RecordToImport['Families']['FamilyNbPoweredMembers'] > 0)
                     {
                         $AnnualPriceOff = $CONF_CONTRIBUTIONS_ANNUAL_AMOUNTS[$CurrentSchoolYear][1];
                     }
                 }

                 // Compute the price of the annual contribution
                 $AnnualContributionBalance = ADMIN_DEFAULT_FAMILY_ANNUAL_CONTRIBUTION_BALANCE;

                 if (isset($CONF_CONTRIBUTIONS_ANNUAL_AMOUNTS[$CurrentSchoolYear][$NbContributions]))
                 {
                     $AnnualContributionBalance = (-1.00 * $CONF_CONTRIBUTIONS_ANNUAL_AMOUNTS[$CurrentSchoolYear][$NbContributions]) + $AnnualPriceOff;
                 }

                 $RecordToImport['Families']['FamilyAnnualContributionBalance'] = $AnnualContributionBalance;
             }

             if (!isset($RecordToImport['Families']['FamilyBalance']))
             {
                 $RecordToImport['Families']['FamilyBalance'] = ADMIN_DEFAULT_FAMILY_BALANCE;
             }

             // We treat the record
             $bContinue = TRUE;
             foreach($RecordToImport as $Table => $aFields)
             {
                 foreach($aFields as $Field => $Value)
                 {
                     // Check if each required value is ok
                     if ((in_array($Field, $CONF_ADMIN_REQUIRED_FIELDS[$Table])) && (($Value === '') || (is_null($Value))))
                     {
                         // Error : required field with an empty value
                         $bContinue = FALSE;
                     }
                 }
             }

             if ($bContinue)
             {
                 // Check if town exists : create or get it's ID
                 $TownID = dbAddTown($DbCon, $RecordToImport['Towns']['TownName'], $RecordToImport['Towns']['TownCode']);
                 if ($TownID > 0)
                 {
                     // Check if the family exists : create or get it's ID
                     $FamilyID = dbAddFamily($DbCon, $RecordToImport['Families']['FamilyDate'], $RecordToImport['Families']['FamilyLastname'], $TownID,
                                             $RecordToImport['Families']['FamilyMainEmail'], $RecordToImport['Families']['FamilySecondEmail'],
                                             $RecordToImport['Families']['FamilyNbMembers'], $RecordToImport['Families']['FamilyNbPoweredMembers'],
                                             $RecordToImport['Families']['FamilyBalance'], '', NULL, $RecordToImport['Families']['FamilySpecialAnnualContribution'],
                                             $RecordToImport['Families']['FamilyMonthlyContributionMode'], $RecordToImport['Families']['FamilyMonthlyNurseryMode'],
                                             $RecordToImport['Families']['FamilyAnnualContributionBalance'], '', '');

                     if ($FamilyID > 0)
                     {
                          // Family created, we check if the family has an account to use the software
                          $SupportMemberID = getTableFieldValueByFieldName($DbCon, "SupportMembers", "SupportMemberID",
                                                                           "SupportMemberLastname", "=",
                                                                           $RecordToImport['Families']['FamilyLastname'],
                                                                           "SupportMemberID");
                          if (empty($SupportMemberID))
                          {
                              $SupportMemberLastname = str_replace(array('/'), array('-'), $RecordToImport['Families']['FamilyLastname']);
                              $SupportMemberFirstname = "-";
                              $SupportMemberPhone = "";

                              $SupportMemberID = dbAddSupportMember($DbCon, $SupportMemberLastname, $SupportMemberFirstname,
                                                                    $RecordToImport['Families']['FamilyMainEmail'],
                                                                    ADMIN_DEFAULT_SUPPORT_MEMBER_STATE_ID, $SupportMemberPhone, 1, $FamilyID);

                              if ($SupportMemberID)
                              {
                                  // Create a login and password
                                  $sLogin = md5(strtolower($SupportMemberLastname));
                                  $sPassword = md5(strtolower(ADMIN_DEFAULT_SUPPORT_MEMBER_PWD));
                                  dbSetLoginPwdSupportMember($DbCon, $SupportMemberID, $sLogin, $sPassword);

                                  // Create a webservice key
                                  $sWebServiceKey = md5(strtolower($SupportMemberLastname).$SupportMemberID);
                                  dbSetWebServiceKeySupportMember($DbCon, $SupportMemberID, $sWebServiceKey);
                              }
                          }

                          $ArrayChildren = dbSearchChild($DbCon, array(
                                                                       "FamilyID" => array($FamilyID),
                                                                       "ChildFirstname" => $RecordToImport['Children']['ChildFirstname']
                                                                      ), "ChildFirstname", 1, 0);

                          if ((isset($ArrayChildren['ChildID'])) && (!empty($ArrayChildren['ChildID'])))
                          {
                              // The child already exists
                              $ChildID = $ArrayChildren['ChildID'][0];
                          }
                          else
                          {
                              // We must create the child
                              $ChildID = dbAddChild($DbCon, $RecordToImport['Children']['ChildSchoolDate'],
                                                    $RecordToImport['Children']['ChildFirstname'], $FamilyID,
                                                    $RecordToImport['Children']['ChildGrade'], $RecordToImport['Children']['ChildClass'],
                                                    $RecordToImport['Children']['ChildWithoutPork'], NULL);
                          }

                          if ($ChildID > 0)
                          {
                              // Get the current history of the child
                              $ArrayHistoChild = getHistoLevelsChild($DbCon, $ChildID, "HistoLevelChildYear DESC,
                                                                     HistoLevelChildID DESC");

                              if ((isset($ArrayHistoChild['HistoLevelChildID'])) && (!empty($ArrayHistoChild['HistoLevelChildID'])))
                              {
                                  // We check if the first entry (so, the current state of the child) = current data about the child
                                  if ($ArrayHistoChild['HistoLevelChildYear'][0] == $CurrentSchoolYear)
                                  {
                                      $HistoChildID = dbUpdateHistoLevelChild($DbCon, $ArrayHistoChild['HistoLevelChildID'][0], $ChildID,
                                                                              $ArrayHistoChild['HistoLevelChildYear'][0],
                                                                              $RecordToImport['Children']['ChildGrade'],
                                                                              $RecordToImport['Children']['ChildClass'],
                                                                              $RecordToImport['Children']['ChildWithoutPork']);
                                  }
                                  elseif ($CurrentSchoolYear > $ArrayHistoChild['HistoLevelChildYear'][0])
                                  {
                                      $HistoChildID = dbAddHistoLevelChild($DbCon, $ChildID, $CurrentSchoolYear,
                                                                           $RecordToImport['Children']['ChildGrade'],
                                                                           $RecordToImport['Children']['ChildClass'],
                                                                           $RecordToImport['Children']['ChildWithoutPork']);
                                  }
                              }
                              else
                              {
                                  // No history, we create one entry for this school year
                                  $HistoChildID = dbAddHistoLevelChild($DbCon, $ChildID, $CurrentSchoolYear,
                                                                       $RecordToImport['Children']['ChildGrade'],
                                                                       $RecordToImport['Children']['ChildClass'],
                                                                       $RecordToImport['Children']['ChildWithoutPork']);
                              }

                              if ($HistoChildID > 0)
                              {
                                  echo "<p>Famille <b>".$RecordToImport['Families']['FamilyLastname']
                                       ."</b>, Enfant <b>".$RecordToImport['Children']['ChildFirstname']
                                       ."</b>, Commune <b>".$RecordToImport['Towns']['TownName']."</b> trait�.</p>\n";
                              }
                          }
                     }
                     else
                     {
                         echo "<p style=\"color: #f00;\">ERREUR de cr�ation en BD sur Famille <b>".$RecordToImport['Families']['FamilyLastname']
                              ."</b>, Enfant <b>".$RecordToImport['Children']['ChildFirstname']
                              ."</b>, Commune <b>".$RecordToImport['Towns']['TownName']."</b>.</p>\n";
                     }
                 }
             }
             else
             {
                 echo "<p style=\"color: #f00;\">ERREUR sur Famille <b>".$RecordToImport['Families']['FamilyLastname']
                      ."</b>, Enfant <b>".$RecordToImport['Children']['ChildFirstname']
                      ."</b>, Commune <b>".$RecordToImport['Towns']['TownName']."</b>.</p>\n";
             }
         }
     }
 }
 else
 {
     echo "<p style=\"color: #f00;\">ERREUR : le fichier d'import ne correspond pas au format attendu !</p>\n";
 }

 dbDisconnection($DbCon);
?>
