1) Ajouter le champ FamilyMonthlyNurseryMode à la table Families :
ALTER TABLE `Families` ADD `FamilyMonthlyNurseryMode` TINYINT UNSIGNED NOT NULL DEFAULT '0' AFTER `FamilyMonthlyContributionMode`;

2) Ajouter le champ HistoFamilyMonthlyNurseryMode à la table HistoFamilies :
ALTER TABLE `HistoFamilies` ADD `HistoFamilyMonthlyNurseryMode` TINYINT UNSIGNED NOT NULL DEFAULT '0' AFTER `HistoFamilyMonthlyContributionMode`;

3) Ajouter le champ BillOtherAmount à la table Bills :
ALTER TABLE `Bills` ADD `BillOtherAmount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `BillNurseryNbDelays`;

4) Ajouter les champs ForumCategoryType et ForumCategoryTypeParams dans la table ForumCategories : 
ALTER TABLE `ForumCategories` ADD `ForumCategoryType` TINYINT UNSIGNED NOT NULL DEFAULT '0' AFTER `ForumCategoryName`;
ALTER TABLE `ForumCategories` ADD `ForumCategoryTypeParams` VARCHAR(255) NULL DEFAULT NULL ;

5) MAJ fichiers php/js/css