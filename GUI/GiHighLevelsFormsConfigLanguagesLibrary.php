<?php
/* Copyright (C) 2012 Calandreta Del Pa�s Murethin
 *
 * This file is part of CanteenCalandreta.
 *
 * CanteenCalandreta is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CanteenCalandreta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CanteenCalandreta; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * Interface module : XHTML Graphic high level forms library used to display translations of messages
 *
 * @author Christophe Javouhey
 * @version 3.9
 * @since 2022-10-18
 */


/**
 * Display the form to submit a new translation of message or update a translation of message, in the
 * current row of the table of the web page, in the graphic interface in XHTML
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-18
 *
 * @param $DbConnection             DB object             Object of the opened database connection
 * @param $ConfigLanguageID         String                ID of the translation of message to display [1..n]
 * @param $ProcessFormPage          String                URL of the page which will process the form
 * @param $AccessRules              Array of Integers     List used to select only some support members
 *                                                        allowed to create, update or view translations of messages
 */
 function displayDetailsConfigLanguageForm($DbConnection, $ConfigLanguageID, $ProcessFormPage, $AccessRules = array())
 {
     // The supporter must be logged,
     if (isSet($_SESSION["SupportMemberID"]))
     {
         /// The supporter must be allowed to create or update a translation of message
         $cUserAccess = FCT_ACT_NO_RIGHTS;
         if (empty($SupportMemberStateID))
         {
             // Creation mode
             if ((isset($AccessRules[FCT_ACT_CREATE])) && (in_array($_SESSION["SupportMemberStateID"], $AccessRules[FCT_ACT_CREATE])))
             {
                 $cUserAccess = FCT_ACT_CREATE;
             }
         }
         else
         {
             if ((isset($AccessRules[FCT_ACT_UPDATE])) && (in_array($_SESSION["SupportMemberStateID"], $AccessRules[FCT_ACT_UPDATE])))
             {
                 // Update mode
                 $cUserAccess = FCT_ACT_UPDATE;
             }
             elseif ((isset($AccessRules[FCT_ACT_READ_ONLY])) && (in_array($_SESSION["SupportMemberStateID"], $AccessRules[FCT_ACT_READ_ONLY])))
             {
                 // Read mode
                 $cUserAccess = FCT_ACT_READ_ONLY;
             }
             elseif ((isset($AccessRules[FCT_ACT_PARTIAL_READ_ONLY])) && (in_array($_SESSION["SupportMemberStateID"], $AccessRules[FCT_ACT_PARTIAL_READ_ONLY])))
             {
                 // Partial read mode
                 $cUserAccess = FCT_ACT_PARTIAL_READ_ONLY;
             }
         }

         if (in_array($cUserAccess, array(FCT_ACT_CREATE, FCT_ACT_UPDATE, FCT_ACT_READ_ONLY, FCT_ACT_PARTIAL_READ_ONLY)))
         {
             // Open a form
             openForm("FormDetailsConfigLanguage", "post", "$ProcessFormPage?".$GLOBALS["QUERY_STRING"], "",
                      "VerificationConfigLanguage('".$GLOBALS["LANG_ERROR_MANDORY_FIELDS"]."')");

             // <<< ConfigLanguageID >>>
             if ($ConfigLanguageID == 0)
             {
                 // Define default values to create the new config language
                 $Reference = "";
                 $ConfigLanguageRecord = array(
                                               "ConfigLanguageName" => '',
                                               "ConfigLanguageLang" => '',
                                               "ConfigLanguageValue" => ''
                                              );
             }
             else
             {
                 if (isExistingConfigLanguage($DbConnection, $ConfigLanguageID))
                 {
                     // We get the details of the config parameter
                     $ConfigLanguageRecord = getTableRecordInfos($DbConnection, "ConfigLanguages", $ConfigLanguageID);
                     $Reference = $ConfigLanguageID;
                 }
                 else
                 {
                     // Error, the config parameter doesn't exist
                     $ConfigLanguageID = 0;
                     $Reference = "";
                     $ConfigLanguageRecord = array(
                                                   "ConfigLanguageName" => '',
                                                   "ConfigLanguageLang" => '',
                                                   "ConfigLanguageValue" => ''
                                                  );
                 }
             }

             // Display the table (frame) where the form will take place
             $FrameTitle = $GLOBALS["LANG_CONFIG_LANGUAGE"];
             if (!empty($ConfigLanguageID))
             {
                 $FrameTitle .= " ($Reference)";
             }

             openStyledFrame($FrameTitle, "Frame", "Frame", "DetailsNews");

             // <<< ConfigParameterName INPUTFIELD >>>
             switch($cUserAccess)
             {
                 case FCT_ACT_READ_ONLY:
                 case FCT_ACT_PARTIAL_READ_ONLY:
                     $sLanguageName = stripslashes($ConfigLanguageRecord["ConfigLanguageName"]);
                     break;

                 case FCT_ACT_CREATE:
                 case FCT_ACT_UPDATE:
                     $sLanguageName = generateInputField("sLanguageName", "text", "255", "70", $GLOBALS["LANG_CONFIG_LANGUAGE_NAME_TIP"],
                                                         $ConfigLanguageRecord["ConfigLanguageName"]);
                     break;
             }

             // <<< ConfigLanguageLang INPUTFIELD >>>
             switch($cUserAccess)
             {
                 case FCT_ACT_READ_ONLY:
                 case FCT_ACT_PARTIAL_READ_ONLY:
                     $sLanguageLang = stripslashes($ConfigLanguageRecord["ConfigLanguageLang"]);
                     break;

                 case FCT_ACT_CREATE:
                 case FCT_ACT_UPDATE:
                     $sLanguageLang = generateInputField("sLanguageLang", "text", "2", "2", $GLOBALS["LANG_CONFIG_LANGUAGE_LANG_TIP"],
                                                         $ConfigLanguageRecord["ConfigLanguageLang"]);
                     break;
             }

             // <<< ConfigLanguageValue INPUTFIELD >>>
             switch($cUserAccess)
             {
                 case FCT_ACT_READ_ONLY:
                 case FCT_ACT_PARTIAL_READ_ONLY:
                     $sLanguageValue = stripslashes(nullFormatText($ConfigLanguageRecord["ConfigLanguageValue"]));
                     break;

                 case FCT_ACT_CREATE:
                 case FCT_ACT_UPDATE:
                     $sLanguageValue = generateTextareaField("sLanguageValue", 15, 70, $GLOBALS["LANG_CONFIG_LANGUAGE_VALUE_TIP"],
                                                             invFormatText($ConfigLanguageRecord["ConfigLanguageValue"]));
                     break;
             }

             // Display the form
             echo "<table cellspacing=\"0\" cellpadding=\"0\">\n<tr>\n\t<td class=\"Label\">".$GLOBALS["LANG_CONFIG_LANGUAGE_NAME"]."*</td><td class=\"Value\">$sLanguageName</td>\n</tr>\n";
             echo "<tr>\n\t<td class=\"Label\">".$GLOBALS["LANG_CONFIG_LANGUAGE_LANG"]."*</td><td class=\"Value\">$sLanguageLang</td>\n</tr>\n";
             echo "<tr>\n\t<td class=\"Label\">".$GLOBALS['LANG_CONFIG_LANGUAGE_VALUE']."</td><td class=\"Value\">$sLanguageValue</td>\n</tr>\n";
             echo "</table>\n";

             insertInputField("hidConfigLanguageID", "hidden", "", "", "", $ConfigLanguageID);
             closeStyledFrame();

             switch($cUserAccess)
             {
                 case FCT_ACT_CREATE:
                 case FCT_ACT_UPDATE:
                     // We display the buttons
                     echo "<table class=\"validation\">\n<tr>\n\t<td>";
                     insertInputField("bSubmit", "submit", "", "", $GLOBALS["LANG_SUBMIT_BUTTON_TIP"], $GLOBALS["LANG_SUBMIT_BUTTON_CAPTION"]);
                     echo "</td><td class=\"FormSpaceBetweenButtons\"></td><td>";
                     insertInputField("bReset", "reset", "", "", $GLOBALS["LANG_RESET_BUTTON_TIP"], $GLOBALS["LANG_RESET_BUTTON_CAPTION"]);
                     echo "</td>\n</tr>\n</table>\n";
                     break;
             }

             closeForm();
         }
         else
         {
             // The supporter isn't allowed to create or update a config language
             openParagraph('ErrorMsg');
             echo $GLOBALS["LANG_ERROR_NOT_ALLOWED_TO_CREATE_OR_UPDATE"];
             closeParagraph();
         }
     }
     else
     {
         // The supporter isn't logged
         openParagraph('ErrorMsg');
         echo $GLOBALS["LANG_ERROR_NOT_LOGGED"];
         closeParagraph();
     }
 }


/**
 * Display the form to search a translation of message in the current web page, in the graphic interface in XHTML
 *
 * @author Christophe Javouhey
 * @version 1.0
 * @since 2022-10-18
 *
 * @param $DbConnection                DB object            Object of the opened database connection
 * @param $TabParams                   Array of Strings     search criterion used to find some translation of messages
 * @param $ProcessFormPage             String               URL of the page which will process the form allowing to find and to sort
 *                                                          the table of the translations of messages found
 * @param $Page                        Integer              Number of the Page to display [1..n]
 * @param $SortFct                     String               Javascript function used to sort the table
 * @param $OrderBy                     Integer              n� Criteria used to sort the translations of messages. If < 0, DESC is used,
 *                                                          otherwise ASC is used
 * @param $DetailsPage                 String               URL of the page to display details about a translation of message.
 *                                                          This string can be empty
 * @param $AccessRules                 Array of Integers    List used to select only some support members
 *                                                          allowed to create or update translation of messages
 */
 function displaySearchConfigLanguagessForm($DbConnection, $TabParams, $ProcessFormPage, $Page = 1, $SortFct = '', $OrderBy = 0, $DetailsPage = '', $AccessRules = array())
 {
     if (isSet($_SESSION["SupportMemberID"]))
     {
         // The supporter must be allowed to access to translation of messages list
         $cUserAccess = FCT_ACT_NO_RIGHTS;
         $bCanDelete = FALSE;
         if ((isset($AccessRules[FCT_ACT_CREATE])) && (in_array($_SESSION["SupportMemberStateID"], $AccessRules[FCT_ACT_CREATE])))
         {
             // Write mode
             $cUserAccess = FCT_ACT_CREATE;
             $bCanDelete = TRUE;
         }
         elseif ((isset($AccessRules[FCT_ACT_UPDATE])) && (in_array($_SESSION["SupportMemberStateID"], $AccessRules[FCT_ACT_UPDATE])))
         {
             // Write mode
             $cUserAccess = FCT_ACT_UPDATE;
             $bCanDelete = TRUE;
         }
         elseif ((isset($AccessRules[FCT_ACT_READ_ONLY])) && (in_array($_SESSION["SupportMemberStateID"], $AccessRules[FCT_ACT_READ_ONLY])))
         {
             // Read mode
             $cUserAccess = FCT_ACT_READ_ONLY;
         }
         elseif ((isset($AccessRules[FCT_ACT_PARTIAL_READ_ONLY])) && (in_array($_SESSION["SupportMemberStateID"], $AccessRules[FCT_ACT_PARTIAL_READ_ONLY])))
         {
             // Partial read mode
             $cUserAccess = FCT_ACT_PARTIAL_READ_ONLY;
         }

         if (in_array($cUserAccess, array(FCT_ACT_CREATE, FCT_ACT_UPDATE, FCT_ACT_READ_ONLY, FCT_ACT_PARTIAL_READ_ONLY)))
         {
             // Open a form
             openForm("FormSearchConfigLanguage", "post", "$ProcessFormPage", "", "");

             // Display the table (frame) where the form will take place
             openStyledFrame($GLOBALS["LANG_SEARCH"], "Frame", "Frame", "SearchFrame");

             // ConfigLanguageName input text
             $sDefaultValue = stripslashes(strip_tags(existedPOSTFieldValue("sLanguageName",
                                                                            stripslashes(existedGETFieldValue("sLanguageName", "")))));
             if ((empty($sDefaultValue)) && (isset($TabParams['ConfigLanguageName'])) && (!empty($TabParams['ConfigLanguageName'])))
             {
                 $sDefaultValue = $TabParams['ConfigLanguageName'];
             }
             $sLanguageName = generateInputField("sLanguageName", "text", "255", "25", $GLOBALS["LANG_CONFIG_LANGUAGE_NAME_TIP"], $sDefaultValue);

             // ConfigLanguageLang input text
             $sDefaultValue = stripslashes(strip_tags(existedPOSTFieldValue("sLanguageLang",
                                                                            stripslashes(existedGETFieldValue("sLanguageLang", "")))));
             if ((empty($sDefaultValue)) && (isset($TabParams['ConfigLanguageLang'])) && (!empty($TabParams['ConfigLanguageLang'])))
             {
                 $sDefaultValue = $TabParams['ConfigLanguageLang'];
             }
             $sLanguageLang = generateInputField("sLanguageLang", "text", "2", "2", $GLOBALS["LANG_CONFIG_LANGUAGE_LANG_TIP"], $sDefaultValue);

             // Display the form
             echo "<table id=\"ConfigLanguagesList\" cellspacing=\"0\" cellpadding=\"0\">\n";
             echo "<tr>\n\t<td class=\"Label\">".$GLOBALS["LANG_CONFIG_LANGUAGE_NAME"]."</td><td class=\"Value\">$sLanguageName</td><td class=\"Label\">".$GLOBALS["LANG_CONFIG_LANGUAGE_LANG"]."</td><td class=\"Value\">$sLanguageLang</td>\n</tr>\n";
             echo "</table>\n";

             // Display the hidden fields
             insertInputField("hidOrderByField", "hidden", "", "", "", $OrderBy);
             insertInputField("hidOnPrint", "hidden", "", "", "", zeroFormatValue(existedPOSTFieldValue("hidOnPrint", existedGETFieldValue("hidOnPrint", ""))));
             insertInputField("hidOnExport", "hidden", "", "", "", zeroFormatValue(existedPOSTFieldValue("hidOnExport", existedGETFieldValue("hidOnExport", ""))));
             insertInputField("hidExportFilename", "hidden", "", "", "", existedPOSTFieldValue("hidExportFilename", existedGETFieldValue("hidExportFilename", "")));
             closeStyledFrame();

             echo "<table class=\"validation\">\n<tr>\n\t<td>";
             insertInputField("bSubmit", "submit", "", "", $GLOBALS["LANG_SUBMIT_BUTTON_TIP"], $GLOBALS["LANG_SUBMIT_BUTTON_CAPTION"]);
             echo "</td><td class=\"FormSpaceBetweenButtons\"></td><td>";
             insertInputField("bReset", "reset", "", "", $GLOBALS["LANG_RESET_BUTTON_TIP"], $GLOBALS["LANG_RESET_BUTTON_CAPTION"]);
             echo "</td>\n</tr>\n</table>\n";

             closeForm();

             // The supporter has executed a search
             $NbTabParams = count($TabParams);
             if ($NbTabParams > 0)
             {
                 displayBR(2);

                 $ArrayCaptions = array($GLOBALS["LANG_REFERENCE"], $GLOBALS["LANG_CONFIG_LANGUAGE_NAME"],
                                        $GLOBALS['LANG_CONFIG_LANGUAGE_LANG'], $GLOBALS['LANG_CONFIG_LANGUAGE_VALUE']);
                 $ArraySorts = array("ConfigLanguageID", "ConfigLanguageName", "ConfigLanguageLang", "ConfigLanguageValue");

                 // Order by instruction
                 if ((abs($OrderBy) <= count($ArraySorts)) && ($OrderBy != 0))
                 {
                     $StrOrderBy = $ArraySorts[abs($OrderBy) - 1];
                     if ($OrderBy < 0)
                     {
                         $StrOrderBy .= " DESC";
                     }
                 }
                 else
                 {
                     $StrOrderBy = "ConfigLanguageName";
                 }

                 // We launch the search
                 $NbRecords = getNbdbSearchConfigLanguages($DbConnection, $TabParams);
                 if ($NbRecords > 0)
                 {
                     // To get only translations of messages of the page
                     $ArrayRecords = dbSearchConfigLanguages($DbConnection, $TabParams, $StrOrderBy, $Page,
                                                             $GLOBALS["CONF_RECORDS_PER_PAGE"]);
                     /*openParagraph('toolbar');
                     displayStyledLinkText($GLOBALS["LANG_PRINT"], "javascript:PrintWebPage()", "", "", "");
                     echo "&nbsp;&nbsp;";
                     displayStyledLinkText($GLOBALS["LANG_EXPORT_TO_XML_FILE"], "javascript:ExportWebPage('".$GLOBALS["CONF_EXPORT_XML_RESULT_FILENAME"].time().".xml')", "", "", "");
                     echo "&nbsp;&nbsp;";
                     displayStyledLinkText($GLOBALS["LANG_EXPORT_TO_CSV_FILE"], "javascript:ExportWebPage('".$GLOBALS["CONF_EXPORT_CSV_RESULT_FILENAME"].time().".csv')", "", "", "");
                     closeParagraph(); */

                     // There are some translations of messages found
                     foreach($ArrayRecords["ConfigLanguageID"] as $i => $CurrentValue)
                     {
                         if (empty($DetailsPage))
                         {
                             // We display the config language ID
                             $ArrayData[0][] = $ArrayRecords["ConfigLanguageID"][$i];
                         }
                         else
                         {
                             // We display the config language ID with a hyperlink
                             $ArrayData[0][] = generateAowIDHyperlink($ArrayRecords["ConfigLanguageID"][$i],
                                                                      $ArrayRecords["ConfigLanguageID"][$i],
                                                                      $DetailsPage, $GLOBALS["LANG_VIEW_DETAILS_INSTRUCTIONS"],
                                                                      "", "_blank");
                         }

                         $ArrayData[1][] = $ArrayRecords["ConfigLanguageName"][$i];
                         $ArrayData[2][] = $ArrayRecords["ConfigLanguageLang"][$i];
                         $ArrayData[3][] = $ArrayRecords["ConfigLanguageValue"][$i];

                         // Hyperlink to delete the config language if allowed
                         if ($bCanDelete)
                         {
                             $ArrayData[4][] = generateStyledPictureHyperlink($GLOBALS["CONF_DELETE_ICON"],
                                                                              "DeleteConfigLanguage.php?Cr=".md5($CurrentValue)."&amp;Id=$CurrentValue&amp;Return=$ProcessFormPage&amp;RCr=".md5($CurrentValue)."&amp;RId=$CurrentValue",
                                                                              $GLOBALS["LANG_DELETE"], 'Affectation');
                         }
                     }

                     // Display the table which contains the config languages found
                     $ArraySortedFields = array("1", "2", "3", "4");
                     if ($bCanDelete)
                     {
                         $ArrayCaptions[] = "";
                         $ArraySorts[] = "";
                         $ArraySortedFields[] = "";
                     }

                     displayStyledTable($ArrayCaptions, $ArraySortedFields, $SortFct, $ArrayData, '', '', '', '',
                                        array(), $OrderBy, array(0 => '', 1 => 'textLeft'));

                     // Display the previous and next links
                     $NoPage = 0;
                     if ($Page <= 1)
                     {
                         $PreviousLink = '';
                     }
                     else
                     {
                         $NoPage = $Page - 1;

                         // We get the parameters of the GET form or the POST form
                         if (count($_POST) == 0)
                         {
                             // GET form
                             if (count($_GET) == 0)
                             {
                                 // No form submitted
                                 $PreviousLink = "$ProcessFormPage?Pg=$NoPage&amp;Ob=$OrderBy";
                             }
                             else
                             {
                                 // GET form
                                 $PreviousLink = "$ProcessFormPage?";
                                 foreach($_GET as $i => $CurrentValue)
                                 {
                                     if ($i == "Pg")
                                     {
                                         $CurrentValue = $NoPage;
                                     }
                                     $PreviousLink .= "&amp;$i=".urlencode(str_replace(array("&", "+"), array("&amp;", "@@@"), $CurrentValue));
                                 }
                             }
                         }
                         else
                         {
                             // POST form
                             $PreviousLink = "$ProcessFormPage?Pg=$NoPage&amp;Ob=$OrderBy";
                             foreach($_POST as $i => $CurrentValue)
                             {
                                 if (is_array($CurrentValue))
                                 {
                                     // The value is an array
                                     $CurrentValue = implode("_", $CurrentValue);
                                 }

                                 $PreviousLink .= "&amp;$i=".urlencode(str_replace(array("&", "+"), array("&amp;", "@@@"), $CurrentValue));
                             }
                         }
                     }

                     if ($Page < ceil($NbRecords / $GLOBALS["CONF_RECORDS_PER_PAGE"]))
                     {
                         $NoPage = $Page + 1;

                         // We get the parameters of the GET form or the POST form
                         if (count($_POST) == 0)
                         {
                             if (count($_GET) == 0)
                             {
                                 // No form submitted
                                 $NextLink = "$ProcessFormPage?Pg=$NoPage&amp;Ob=$OrderBy";
                             }
                             else
                             {
                                 // GET form
                                 $NextLink = "$ProcessFormPage?";
                                 foreach($_GET as $i => $CurrentValue)
                                 {
                                     if ($i == "Pg")
                                     {
                                         $CurrentValue = $NoPage;
                                     }
                                     $NextLink .= "&amp;$i=".urlencode(str_replace(array("&", "+"), array("&amp;", "@@@"), $CurrentValue));
                                 }
                             }
                         }
                         else
                         {
                             // POST form
                             $NextLink = "$ProcessFormPage?Pg=$NoPage&amp;Ob=$OrderBy";
                             foreach($_POST as $i => $CurrentValue)
                             {
                                 if (is_array($CurrentValue))
                                 {
                                     // The value is an array
                                     $CurrentValue = implode("_", $CurrentValue);
                                 }

                                 $NextLink .= "&amp;$i=".urlencode(str_replace(array("&", "+"), array("&amp;", "@@@"), $CurrentValue));
                             }
                         }
                     }
                     else
                     {
                         $NextLink = '';
                     }

                     displayPreviousNext("&nbsp;".$GLOBALS["LANG_PREVIOUS"], $PreviousLink, $GLOBALS["LANG_NEXT"]."&nbsp;", $NextLink,
                                         '', $Page, ceil($NbRecords / $GLOBALS["CONF_RECORDS_PER_PAGE"]));

                     openParagraph('nbentriesfound');
                     echo $GLOBALS['LANG_NB_RECORDS_FOUND'].$NbRecords;
                     closeParagraph();
                 }
                 else
                 {
                     // No config parameter found
                     openParagraph('nbentriesfound');
                     echo $GLOBALS['LANG_NO_RECORD_FOUND'];
                     closeParagraph();
                 }
             }
         }
         else
         {
             // The supporter isn't allowed to view the list of config languages
             openParagraph('ErrorMsg');
             echo $GLOBALS["LANG_ERROR_NOT_ALLOWED_TO_CREATE_OR_UPDATE"];
             closeParagraph();
         }
     }
     else
     {
         // The user isn't logged
         openParagraph('ErrorMsg');
         echo $GLOBALS["LANG_ERROR_NOT_LOGGED"];
         closeParagraph();
     }
 }
?>